export interface Queue{
      Id                : number,
      QueueDate         : Date,
      CompanyId         : number,
      BranchId          : number,
      AppointmentId     : number,
      QueueNo           : number,
      ServiceGroupId    : number,
      GroupCode         : number,
      GroupName         : string,
      ServiceTypeId     : number,
      TypeCode          : string,
      TypeName          : string,
      ServiceDate       : Date,
      RegNo             : string,
      ChassisNo         : string,
      CustomerName      : string,
      MobileNo          : string,
      CSAUserId         : number,
      CSAName           : string,
      ArrivedOn         : Date,
      RONo              : string,
      RODate            : Date,
      PictureUrl        : string,
      IsVIP             : boolean,
      IsPriorityPlus    : boolean,
      IsKeyDrop         : boolean,
      IsTestDrive       : boolean,
      IsDeleted         : boolean,
      RefQueueId        : number,
      InsertDate        : Date,
      InsertUserId      : number,
      InsertUserName    : string,
      UpdateDate        : Date,
      ServeStartOn      : Date,
      ServeEndOn        : Date,
      UpdateUserId      : number,
      UpdateUserName    : string,
      Status            : string,
      ApptId            : number,
      ApptTime          : string,
      QueuedOn          : Date,
      ServeTime         : string,
      TimeWaited        : number,
      Remarks           : string
}

export class Param{
      rootcaller: string;
      caller:string;
      groupcd:string;
      statuscd:string;
      csauserid:number;
    }
    
export class VehInfo{
      ApptDate: Date;
      BodyStyle: string;
      CSAName: string;
      CSAUserId: number;
      ChassisNo: string;
      Color: string;
      CustomerName: string;
      EngineCap: number
      ExtWtyExpDate: Date
      MakeName: string
      ModelName: string
      ModelRange: string
      ModelYear: number
      ServiceTypeId: number
      WtyExpDate: Date
    }
export class AppSession{
      public IsAuthenticated   :Boolean;
      public TokenId           : String;
      public User              :User
      constructor(){
            this.IsAuthenticated = false;
            this.TokenId = null;            
            this.User = new User();
      }
}

class User{
      UserId            : number;
      Username          : string;
      UserCode          : string;
      DisplayName       : string;
      Email             : string;
      Source            : string;
      RoleCode          : string;
      RoleName          : string;
      CompanyId         : number;
	CompanyCode       : string;
      CompanyName       : string;
	BranchId          : number;
	BranchCode        : string;
	BranchName        : string
      constructor(){}
}
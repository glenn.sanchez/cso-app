export interface CSA{
    UserId: number,
    Username: string,
    DisplayName: string,
    CounterCode: string,
    CounterDesc: string,
    Status: string,
    TimeDiff: number,
    WaitTime: number,
    TotalAssigned: number
}

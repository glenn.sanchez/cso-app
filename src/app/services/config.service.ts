import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Observable } from 'rxjs';
import {Globals} from './globals.service';

@Injectable({
  providedIn: 'root'
})

export class ConfigService {
  endpoint = Globals.EndPoint;
  constructor(private http:HttpClient) { }

  async initSetup() {

    this.getUserServices().subscribe(
      (items:any[])=>{
          Globals.ServiceTypes = items;
      });
  }

  public getUserServices(): Observable<any> {
    var param = {
                  TokenId: Globals.Session.TokenId
                };

    return this.http.post(this.endpoint + "/api/userservices/get",param);
  }
}


import { Injectable } from '@angular/core';
import { Globals } from './globals.service';
import { Observable } from 'rxjs';
import { HttpClient } from  '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApicamService {
  endpoint = Globals.EndPoint;
  constructor(private http:HttpClient) { }

  public getFrame(): Observable<any> {
    var param = {
                  TokenId: Globals.Session.TokenId
                };
                
    return this.http.post(this.endpoint + "/api/ipcam/snap",param);
  }

}



import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './globals.service';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';
import { Device } from '@ionic-native/device/ngx';

import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  private _manufacturer:string;
  private _platform:string;
  private _uuid:string;
  private _model:string;
  private _serial:string;
  private _ipaddress:string;

  endpoint:string = Globals.EndPoint;
  constructor(private http:HttpClient, 
              private platform:Platform,
              private device:Device,
              private networkInterface: NetworkInterface) { 

      if (this.platform.is("cordova")){

        this._manufacturer = this.device.manufacturer;
        this._platform = this.device.platform;
        this._uuid = this.device.uuid;
        this._model = this.device.model;
        this._serial = this.device.serial;        
        
        this.networkInterface.getWiFiIPAddress().then((ip)=>{
          this._ipaddress=ip;
        });

      }
  }


  public login(username:string,password:string): Observable<any> {

    var param = {
                  UserName: username, 
                  Password: password,
                  Manufacturer: this._manufacturer,
                  Platform: this._platform,
                  DeviceId: this._uuid,
                  Model: this._model,
                  SerialNo: this._serial,
                  IPAddress: this._ipaddress
                };

    return this.http.post(this.endpoint + "/api/security/login",param);

  }


  public logout(){
    var param = {
      "Username": Globals.Session.User.Username,
      "LoginUsername": Globals.Session.User.Username
    };

    return this.http.post(this.endpoint + "/api/security/logout",param);

  }
  public setPassword(oldpassword:string,newpassword): Observable<any> {

    var param = {
                  "TokenId": Globals.Session.TokenId,
                  "OldPassword": oldpassword,
                  "NewPassword": newpassword
                };

    return this.http.post(this.endpoint + "/api/security/changepassword",param);

  }


  
}

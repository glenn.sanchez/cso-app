import { TestBed } from '@angular/core/testing';

import { ApicamService } from './apicam.service';

describe('ApicamService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApicamService = TestBed.get(ApicamService);
    expect(service).toBeTruthy();
  });
});

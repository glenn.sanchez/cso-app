import { Injectable } from '@angular/core';
import { Queue } from 'src/app/interfaces/queue';
import { AppSession } from '../interfaces/appsession';

@Injectable({
  providedIn: 'root'
})


export class Globals {
  //public static EndPoint      :string = "http://localhost:53892";    
  public static EndPoint      :string = "https://dev.simedarbymotors.com/care";    
  public static Session       :AppSession;
  public static ServiceTypes  :any    = [];
  public static current_queue :Queue;
  public static QueueSession  :string = "QueueSession";

  constructor() {     
  }

  public static isObject(val:any) {
    if (val === null) { return false;}
    return ( (typeof val === 'function') || (typeof val === 'object') );
  }
}

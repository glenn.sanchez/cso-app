import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Globals } from './globals.service';

@Injectable({
  providedIn: 'root'
})
export class ToasthandlerService {

  constructor(private toastCtrl:ToastController) { }

  public  async showError(error:any){
    var errorText;

    if (Globals.isObject(error)) {
      errorText="Unable to connect to server.";
    }else{
      errorText = error;
    }
  
    const toast = await this.toastCtrl.create({
      message : errorText,
      duration: 2000,
      color   : "danger"
    });
    toast.present();
  }
}

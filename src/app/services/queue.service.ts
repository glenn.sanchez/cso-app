import { Injectable } from '@angular/core';
//import { HttpClient,Request} from 'selenium-webdriver/http';
import { HttpClient } from  '@angular/common/http';
import { Observable } from 'rxjs';
import {Globals} from './globals.service';
import { Queue } from '../interfaces/queue';

@Injectable({
  providedIn: 'root'
})
export class QueueService {
  endpoint = Globals.EndPoint;
  constructor( private http:HttpClient) {

  }
  
  public getQueueItems(IsTestDrive:Boolean,IsValet:Boolean,groupCode:string, statusCode:string): Observable<any> {
    var param = {
                  TokenId: Globals.Session.TokenId,                   
                  IsTestDrive: IsTestDrive,
                  IsValet: IsValet,
                  GroupCode:groupCode,
                  StatusCode:statusCode
                };
                
    return this.http.post(this.endpoint + "/api/queue/get",param);
  }

  public getQueueItemDetail(itemId:number): Observable<any> {
    
    var param = {
                  TokenId: Globals.Session.TokenId,
                  Id: itemId
                };

    return this.http.post(this.endpoint + "/api/queue/get",param);
  }

  public getNextQueueItem(GroupCode:string): Observable<any> {
    var param = {
                  TokenId: Globals.Session.TokenId, 
                  CSAUserId: Globals.Session.User.UserId,
                  ServiceGroupCode: GroupCode
                };
                
    return this.http.post(this.endpoint + "/api/queue/next",param);
  }

  public getQueueSummary(): Observable<any> {
    var param = {
                  TokenId: Globals.Session.TokenId, 
                  CSAUserId: Globals.Session.User.UserId,
                };
                
    return this.http.post(this.endpoint + "/api/queue/summary",param);
  }

  public getQueueTemplate():any{
    var item:Queue = {
      Id:null,
      QueueDate: null,
      CompanyId:null,
      BranchId:null,
      AppointmentId:null,
      QueueNo:null,
      ServiceGroupId:null,
      GroupCode:null,
      GroupName:null,
      ServiceTypeId:null,
      TypeCode:null,
      TypeName:null,
      ServiceDate:null, 
      RegNo:null,
      ChassisNo:null,
      CustomerName:null,
      MobileNo:null,
      CSAUserId:null,
      CSAName:"Next Available CSA",
      ArrivedOn:null,
      RONo:null,
      RODate:null,
      PictureUrl:null,
      IsVIP:null,
      IsPriorityPlus:null,
      IsKeyDrop:null,
      IsTestDrive:null,
      IsDeleted:null,
      RefQueueId:null,
      InsertDate:null,
      InsertUserId:null,
      InsertUserName:null,
      UpdateDate:null,
      ServeStartOn:null,
      ServeEndOn:null,
      UpdateUserId:null,
      UpdateUserName:null,
      Status:'Waiting',
      ApptId:null,
      ApptTime:null,
      QueuedOn:null,
      ServeTime:null,
      TimeWaited:null,
      Remarks:null
    }
    return item;
  }

  public setQueueItem(item:Queue): Observable<any> {
    var param = {
            "TokenId": Globals.Session.TokenId,
            "Id": item.Id,
            "ServiceTypeId": item.ServiceTypeId,
            "CSAUserId": item.CSAUserId,
            "CustomerName": item.CustomerName,
            "MobileNo": item.MobileNo,
            "RegNo": item.RegNo,
            "ChassisNo": item.ChassisNo,
            "ServiceDate": item.ServiceDate,
            "RONo": item.RONo,
            "RODate": item.RODate,
            "AppointmentId": item.AppointmentId?item.AppointmentId:null,
            "IsVIP": item.IsVIP,
            "IsKeydrop": item.IsKeyDrop,
            "IsTestdrive": item.IsTestDrive,
            "IsDeleted": item.IsDeleted,
            "Remarks": item.Remarks,
            "RefQueueId": item.RefQueueId?item.RefQueueId:null,
            "Status": item.Status
    };

    return this.http.post(this.endpoint + "/api/queue/set",param);
  }

  public getCSAItems(): Observable<any> {
    var param = {
                  TokenId: Globals.Session.TokenId
                };
                
    return this.http.post(this.endpoint + "/api/onlinecsa/get",param);
  }

  
  public getVehicleInfo(regNo:string,mobileNo:string): Observable<any> {
    var param = {
                  TokenId: Globals.Session.TokenId,
                  RegNo: regNo,
                  MobileNo: mobileNo
                };
                
    return this.http.post(this.endpoint + "/api/vehicle/info/get",param);
  }

}

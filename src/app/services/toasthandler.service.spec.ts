import { TestBed } from '@angular/core/testing';

import { ToasthandlerService } from './toasthandler.service';

describe('ToasthandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToasthandlerService = TestBed.get(ToasthandlerService);
    expect(service).toBeTruthy();
  });
});

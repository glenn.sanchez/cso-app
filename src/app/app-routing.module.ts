import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './pages/login/login.module#LoginPageModule' },  
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'edit-profile', loadChildren: './pages/edit-profile/edit-profile.module#EditProfilePageModule' },
  //{ path: 'home', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule' },
  //{ path: 'calendar', loadChildren: './pages/calendar/calendar.module#CalendarPageModule' },
  { path: 'queue-list', loadChildren: './pages/queue-list/queue-list.module#QueueListPageModule' },
  { path: 'queue-edit', loadChildren: './pages/queue-edit/queue-edit.module#QueueEditPageModule' },
  { path: 'queue-edit/:id', loadChildren: './pages/queue-edit/queue-edit.module#QueueEditPageModule' },
  { path: 'csa-list', loadChildren: './pages/csa-list/csa-list.module#CSAListPageModule' },
  { path: 'testdrive-list', loadChildren: './pages/testdrive-list/testdrive-list.module#TestDriveListPageModule' },
  //{ path: 'calendar', loadChildren: './pages/calendar/calendar.module#CalendarPageModule' },
  { path: 'queue-call', loadChildren: './pages/queue-call/queue-call.module#QueueCallPageModule' },  
  { path: 'queue-call-edit', loadChildren: './pages/queue-call-edit/queue-call-edit.module#QueueCallEditPageModule' },
  { path: 'csaregister', loadChildren: './pages/csaregister/csaregister.module#CSARegisterPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

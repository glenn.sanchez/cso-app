import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
//import { QueueEditPage } from '../../pages/queue-edit/queue-edit.page';

@Component({
  selector: 'popmenu',
  templateUrl: './popmenu.component.html',
  styleUrls: ['./popmenu.component.scss']
})

//modal


export class PopmenuComponent implements OnInit {
  openMenu: Boolean = false;

  constructor(
    public navCtrl: NavController,
    public modCtrl: ModalController
    ) { }

  ngOnInit() {
  }
  


  togglePopupMenu() {
    return this.openMenu = !this.openMenu;
  }

  goToQueueList() {    
    this.navCtrl.navigateForward('queue-list').finally(()=>{
      this.openMenu = false;
    })
  }

  goToQueueAdd() {    
    this.navCtrl.navigateForward('queue-new').finally(()=>{
      this.openMenu = false;
    });
  }

  
  async addItem() {    
    /*
    const modal = await this.modCtrl.create({
      component: QueueEditPage
    });
    
    return await modal.present();
    */
  }
  
}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CSARegisterPage } from './csaregister.page';

describe('CSARegisterPage', () => {
  let component: CSARegisterPage;
  let fixture: ComponentFixture<CSARegisterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CSARegisterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CSARegisterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

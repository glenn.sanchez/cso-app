import { Component, OnInit, ɵConsole} from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController, ModalController, AlertController } from '@ionic/angular';
import {QueueService} from '../../services/queue.service';
import {ApicamService} from '../../services/apicam.service';

import { ImagePage } from '../modal/image/image.page';
import { Globals } from 'src/app/services/globals.service';
import { Queue,Param,VehInfo } from 'src/app/interfaces/queue';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';

@Component({
  selector: 'queue-new',
  templateUrl: './queue-edit.page.html',
  styleUrls: ['./queue-edit.page.scss'],
})


export class QueueEditPage implements OnInit {
  params:any;
  itemId:number;
  services:any;
  item:Queue;
  vehinfo:any;
  imgpath:string;
  blockQueue:Boolean = true;
  comparewith:any;
  srvtypename:string;
  user:any;
  options: StreamingVideoOptions;
  snapProcess:any;
  constructor(
      public navCtrl: NavController,
      public loadingCtrl: LoadingController,
      public toastCtrl: ToastController,
      public modalCtrl:ModalController,
      public qservice:QueueService,
      public ipcam:ApicamService,
      private route:ActivatedRoute,
      private router: Router,
      public alertCtrl:AlertController,
      private streamingMedia: StreamingMedia
  ){

    this.imgpath = Globals.EndPoint;
    this.item = this.qservice.getQueueTemplate();
    this.services = Globals.ServiceTypes;
    this.params = new Param();
    this.vehinfo  = new VehInfo();
    
    let options = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming')},
      orientation: 'landscape',
      shouldAutoClose: true,
      controls: false
    };

    //this.itemId = Number(this.route.snapshot.paramMap.get('id'));
    //console.log(this.services);    
    
  }

  selectService($event:any){
    console.log($event.detail.value,this.item.ServiceTypeId);
    var srv = this.services.find(x => x.Id == this.item.ServiceTypeId);
    if (srv!=null){
      this.srvtypename = srv.TypeName;
    }
        
    //console.log("this.srvtypename",this.srvtypename);
    

    //this.item.ServiceTypeId = $event.detail.value;
    //this.setServiceTypeName();
    //console.log(this.sel_service);

    /*
    if (this.sel_service){
      var srv = this.services.find(x => x.Id == this.sel_service);    
      this.sel_service = srv?srv.TypeName:null;    
    } 
    */   
  }

  setServiceTypeName(){

    
    if (this.item.ServiceTypeId==null){
      this.item.ServiceTypeId = this.services[0].Id;
    }

    var srv = this.services.find(x => x.Id == this.item.ServiceTypeId);
    this.srvtypename = srv.TypeName;
    
  }
  ngOnInit() {
    
  }

  ionViewWillLeave(){
    console.log("wil leave!");
    clearInterval(this.snapProcess);
  }

  ionViewWillEnter(){
    this.route.queryParams.subscribe((res) => {
      this.params=res;
      console.log(res);
      //this.caller = res.caller;
      //this.itemId = res.Id==undefined?null:res.Id;
      //console.log(this.itemId,this.callerroute);
      if (Globals.current_queue!=null){
        console.log('Globals.current_queue!=null');
        this.item = Globals.current_queue;
        this.loopFrames();
      }
      else{
        if(this.params.id){
          console.log('this.params.id');
          this.loadData();
        }else{
          console.log(this.params.id==null);
          this.loopFrames();
        }

      }
      this.setServiceTypeName();
     });
  }

  async loadData(){
    
    const loader = await this.loadingCtrl.create({
      message: "Loading...",
      duration: 2000
    });

    await loader.present();

    this.qservice.getQueueItemDetail(this.params.id).subscribe(
      (item:any)=>{
          loader.dismiss();
          this.item = item[0];
          console.log(this.item);
      },(err)=>{
        loader.dismiss();
        this.showError(err);
      });

  }


  
  async validateEntries(){
    var ok :Boolean = false;
    //console.log(this.item.regno);

    if (this.item.RegNo){
      ok=false;
    }

    if (!ok){
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        //cssClass: 'bg-profile',
        message: 'Some of required fields are missing.',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
    
    return ok;
  }

  async sendData() {
   var ok = this.validateEntries();

   if (!ok) return;
    const loader = await this.loadingCtrl.create({
      message: "Saving...",
      duration: 2000
    });

    await loader.present();
    
    loader.onWillDismiss().then(async l => {
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        cssClass: 'bg-profile',
        message: 'Your profile was was edited!',
        duration: 3000,
        position: 'bottom'
      });

      toast.present();
      this.navCtrl.navigateBack('/queue-list');
    });
  }

  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  editCSA(csaUserId:number){
    Globals.current_queue = this.item;
    let pm = new Param();

    pm.csauserid = this.item.CSAUserId;
    pm.caller= this.params.caller;
    pm.rootcaller= this.params.rootcaller;
    pm.statuscd= this.params.statuscd;
    pm.groupcd= this.params.groupcd;

    //this.navCtrl.navigateForward('/csa-list');
    this.router.navigate(['/csa-list'], {
      queryParams: pm
    });
  }

  customPopoverOptions: any = {
    header: 'Service Type',
    //subHeader: 'Select Appointment Type',
    message: 'State the purpose of your visit'
  };

  closeModal() {
    Globals.current_queue = null;
    //console.log(this.params);
    this.navCtrl.setDirection('back');
    this.router.navigate([this.params.caller], {
      queryParams: this.params
    });

    //this.navCtrl.navigateBack(this.callerroute);
  }

  async confirm(type:string){
    
    switch(type){
      case "Keydrop":{
                      //this.item.IsKeyDrop=true;
                      this.confirmAction("Keydrop",
                        "Mark this queue as keydrop?",
                        {type:type});
                      break;
                     }
      case "TestDrive":{
                        //this.item.IsTestDrive=true;
                        this.confirmAction("Test Drive",
                          "Mark this queue as test drive?",
                          {type:type});
                        break;
                      }
      case "Delete":{
                      //this.item.IsDeleted=true;
                      this.confirmAction("Confirm Delete",
                        "Do you really want to delete this queue?",
                        {type:type});
                      break;
                    }
      case "Complete":{
                       //this.item.Status="Completed";
                       this.confirmAction("Complete",
                        "Do you really want to complete this queue?",
                        {type:type});
                       break;
                      }
      default:{
            this.confirmAction("Confirm Save",
                              "Save changes?",
                              {type:type});
            break;                              
      }                      
    }
  }

  async showError(err:any){
    var errorText;
    
    if (Globals.isObject(err.error)) {
      errorText="Unable to connect to server.";
    }else{
      errorText = err.error;
    }
  
    const toast = await this.toastCtrl.create({
      message : errorText,
      duration: 2000,
      color   : "danger"
    });
    toast.present();
  }

  compareWithFn(o1, o2) {
    console.log(o1,o2);
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  };

  async getVehicleInfo(){
    //console.log(this.item.RegNo,this.item.MobileNo,this.item.RegNo==null this.item.MobileNo);
    if(this.item.RegNo){
      const loader = await this.loadingCtrl.create({
        message: "Searching Vehicle Information...",
        duration:90000
      });
      /*
      if (this.item.Id==null){
        await loader.present();
      }
      */
      this.qservice.getVehicleInfo(this.item.RegNo,this.item.MobileNo).subscribe(
        (item:any)=>{  
            //loader.dismiss();  

            if (item.length>0){
              this.vehinfo = item[0];
            }else{
              this.vehinfo = new VehInfo();
            }

            if (this.item.Id == null){
              this.item.CustomerName    = this.vehinfo.CustomerName;
              this.item.ServiceDate     = this.vehinfo.ApptDate;
              this.item.CSAUserId       = this.vehinfo.CSAUserId;
              this.item.CSAName         = this.vehinfo.CSAName;
              this.item.IsVIP           = this.vehinfo.IsVIP;
              this.item.IsPriorityPlus  = this.vehinfo.IsPriorityPlus;
            }            
        },(err)=>{
          //loader.dismiss();
          this.showError(err);
        });
    }    
  }

  checkValue(event:any){
    //console.log(event.detail.value,this.item.ServiceTypeId);
    this.item.ServiceTypeId = event.detail.value;
  }

  async confirmAction(title:string,message:string,option:any) {
    const alert = await this.alertCtrl.create({
      header: title,
      message:message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',          
        }, {
          text: 'Ok',
          handler: () => {
            this.saveData(option);
          }
        }
      ]
    });

    await alert.present();
  }

  async saveData(option:any){

    switch(option.type){
      case "Keydrop":{this.item.IsKeyDrop=true;break;}
      case "TestDrive":{this.item.IsTestDrive=true;break;}
      case "Delete":{this.item.IsDeleted=true;break;}
      case "Complete":{this.item.Status="Completed";break;}
    }

    const loader = await this.loadingCtrl.create({
      message: "Saving Changes..."
    });        

    await loader.present();
    
    this.qservice.setQueueItem(this.item).subscribe(
      (res:any)=>{
        clearInterval(this.snapProcess);
        loader.dismiss();
        Globals.current_queue = null;
        this.navCtrl.setDirection('back');
        this.router.navigate(['/queue-list'], { queryParams: this.params});        
      },(err)=>{
        console.log(err);
        loader.dismiss();
        this.showError(err);        
      });
  }

  loopFrames(){
    var _self = this;

     console.log(this.item);
     
     if (this.item.Id==null){
      console.log('loopstarts!');
      //clearInterval(this.snapProcess);      
      _self.ipcam.getFrame().subscribe(data=>{      
        _self.item.PictureUrl = data + "?time="+new Date().getTime();
        console.log(_self.item.PictureUrl);
        //console.log(data);
        this.loopFrames();
      });    

      /*
      this.snapProcess=setInterval(function() { 
        console.log('loop!!');        
      }, 500);
      */

     }
  }
}


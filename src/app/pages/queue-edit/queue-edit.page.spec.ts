import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueueEditPage } from './queue-edit.page';

describe('QueueEditPage', () => {
  let component: QueueEditPage;
  let fixture: ComponentFixture<QueueEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueEditPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

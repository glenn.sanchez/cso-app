import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestDriveListPage } from './testdrive-list.page';

describe('TestDriveListPage', () => {
  let component: TestDriveListPage;
  let fixture: ComponentFixture<TestDriveListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestDriveListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestDriveListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

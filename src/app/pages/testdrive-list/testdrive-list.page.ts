import { Component } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  LoadingController,
  ModalController } from '@ionic/angular';
  import { Router } from '@angular/router';

// Modals
import { SearchFilterPage } from '../modal/search-filter/search-filter.page';
import { NotifListPage } from '../modal/notif-list/notif-list.page';
import { ImagePage } from '../modal/image/image.page';
import {QueueService} from '../../services/queue.service';
import { Queue } from 'src/app/interfaces/queue';
import { Globals } from 'src/app/services/globals.service';
//import { QueueEditPage } from '../queue-edit/queue-edit.page';
//import { QueueNewPage } from '../queue-new/queue-new.page';

@Component({
  selector: 'app-list',
  templateUrl: './testdrive-list.page.html',
  styleUrls: ['./testdrive-list.page.scss']
})
export class TestDriveListPage {
  
  searchKey = '';
  items:Queue[] = [];
  

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public qservice:QueueService,
    public loadingCtrl:LoadingController,
    private router: Router
  ) {

  }

  ngOnInit() {

    
  }

  async loadData() {
    const loading = await this.loadingCtrl.create({
      message: 'Loading...',
    });

    await loading.present();

    this.qservice.getQueueItems(true,null,null,null).subscribe(
      (items:any[])=>{
          this.items = items;          
          //console.log(this.items);
          loading.dismiss();
      },err=>{
        this.showError(err);
      });
      
    
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.loadData();
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  async searchFilter () {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }

  async viewItem(itemId:number){    
    //this.navCtrl.navigateForward('queue-edit/' + itemId);
    console.log(itemId);
    this.router.navigate(['/queue-edit'], {
      queryParams: {
                    caller:'/testdrive-list' ,
                    id:itemId
                   }
    });
  }

  async newItem(){
    //this.navCtrl.navigateForward('queue-edit/');
    this.router.navigate(['/queue-edit'], {
      queryParams: {
                    caller:'/testdrive-list' ,
                    id:null
                   }
    });
  }

  async showNotifications () {
    const modal = await this.modalCtrl.create({
      component: NotifListPage
    });
    return await modal.present();
  }

  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  async showError(err:any){
    var errorText;

    if (Globals.isObject(err.error)) {
      errorText="Unable to connect to server.";
    }else{
      errorText = err.error;
    }
  
    const toast = await this.toastCtrl.create({
      message : errorText,
      duration: 2000,
      color   : "danger"
    });
    toast.present();
  }
}

import { Component } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  LoadingController,
  ModalController } from '@ionic/angular';
  import { Router,ActivatedRoute } from '@angular/router';

// Modals
import { SearchFilterPage } from '../modal/search-filter/search-filter.page';
import { NotifListPage } from '../modal/notif-list/notif-list.page';
import { ImagePage } from '../modal/image/image.page';
import {QueueService} from '../../services/queue.service';
import { Queue } from 'src/app/interfaces/queue';
import { Globals } from 'src/app/services/globals.service';

class Param{
  rootcaller: string;
  caller:string;
  groupcd:string;
  statuscd:string;
}

@Component({
  selector: 'queue-list',
  templateUrl: './queue-list.page.html',
  styleUrls: ['./queue-list.page.scss']
})

export class QueueListPage {
  params:Param;
  searchKey = '';
  allitems:Queue[] = [];
  items:Queue[] = [];
  imgpath:string;
  user:any;
  //statuscd:string;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public qservice:QueueService,
    public loadingCtrl:LoadingController,
    private router: Router,
    private route: ActivatedRoute,
    private toastCtrl:ToastController
  ) {
    this.params = new Param();
    this.user = Globals.Session.User;
    this.imgpath = Globals.EndPoint;    
    console.log('params',this.user);  
  }

  ngOnInit() {
  }

  async loadData(groupcd:string,statuscd:string) {

    const loading = await this.loadingCtrl.create({
      message: 'Loading...',
    });

    await loading.present();
  
    this.qservice.getQueueItems(false,null,groupcd,statuscd).subscribe(      
      (items:any[])=>{
          loading.dismiss();
          console.log(items);
          this.allitems = items;
          this.items = items;          
      },(err)=>{
        loading.dismiss();
        this.showError(err);
      });
  }

  filterData(){
    var newArray;

    if (this.searchKey){
      newArray = this.allitems.filter(el=> el.CustomerName.toLowerCase().indexOf(this.searchKey.toLowerCase()) > -1 || 
                                           el.RegNo.toLowerCase().indexOf(this.searchKey.toLowerCase()) > -1 ||
                                           el.CSAName.toLowerCase().indexOf(this.searchKey.toLowerCase()) > -1
                                           );
    }else{
      newArray = this.allitems;
    }
     
    this.items = newArray;
  }
  
  ionViewWillEnter() {
    
    this.menuCtrl.enable(true);
    this.items = null;
    this.route.queryParams.subscribe((res) => {

      this.params.rootcaller = res.rootcaller;
      this.params.groupcd = res.groupcd;      
      this.params.statuscd = res.statuscd;
      
      this.loadData(this.params.groupcd,this.params.statuscd);  
    },err=>{
      console.log("no error");
    });    
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  async searchFilter () {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }

  async viewItem(itemId:number){    
    //this.navCtrl.navigateForward('queue-edit/' + itemId);
    this.router.navigate(['/queue-edit'], {
      queryParams: {
                    rootcaller: this.params.rootcaller ,
                    caller:'/queue-list' ,
                    id:itemId,
                    groupcd: this.params.groupcd,
                    statuscd: this.params.statuscd
                   }
    });
  }

  async newItem(){
    //this.navCtrl.navigateForward('queue-edit/');
    this.router.navigate(['/queue-edit'], {
      queryParams: {
                    rootcaller:this.params.rootcaller,
                    caller:'/queue-list' ,
                    id:null, 
                    groupcd: this.params.groupcd,
                    statuscd: this.params.statuscd
                   }
    });
  }

  closeModal(){
    this.navCtrl.setDirection('back');
    this.router.navigate([this.params.rootcaller], {
      queryParams: this.params
    });
  }
  async showNotifications () {
    const modal = await this.modalCtrl.create({
      component: NotifListPage
    });
    return await modal.present();
  }

  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  async showError(err:any){
    var errorText;

    if (Globals.isObject(err.error)) {
      errorText="Unable to connect to server.";
    }else{
      errorText = err.error;
    }
  
    const toast = await this.toastCtrl.create({
      message : errorText,
      duration: 2000,
      color   : "danger"
    });
    toast.present();
  }

  reRouteTo(route:string) {
    this.navCtrl.navigateForward(route);
  }
  
  reRouteBackTo(route:string) {
    this.navCtrl.navigateBack(route);
  }
}

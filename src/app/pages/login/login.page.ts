import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController,Events } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Globals } from 'src/app/services/globals.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public onLoginForm: FormGroup;
  username:string;
  password:string;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private auth:AuthService,
    public events:Events,
    private storage: Storage
  ) { 
    
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {

    this.onLoginForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  async forgotPass() {
    const alert = await this.alertCtrl.create({
      header: 'Forgot Password?',
      message: 'Enter you email address to send a reset link password.',
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'Email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: async () => {
            const loader = await this.loadingCtrl.create({
              duration: 2000
            });

            loader.present();
            loader.onWillDismiss().then(async l => {
              const toast = await this.toastCtrl.create({
                showCloseButton: true,
                message: 'Email was sended successfully.',
                duration: 3000,
                position: 'bottom'
              });

              toast.present();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  goToRegister() {
    this.navCtrl.navigateRoot('/register');
  }

  async login() {

    const loader = await this.loadingCtrl.create({  
      message: "Checking account..."
    });

    if (!this.username || !this.password){
      this.showError("Please enter username and password.");
      return;
    }

    await loader.present();

    this.auth.login(this.username,this.password).subscribe((res)=>{
        if (res.IsAuthenticated){          
          
          Globals.Session.TokenId         = res.TokenId;
          Globals.Session.IsAuthenticated = res.IsAuthenticated;
          //User Details
          Globals.Session.User.UserId     = res.User.UserId;
          Globals.Session.User.Username   = res.User.Username;
          Globals.Session.User.DisplayName= res.User.DisplayName;
          Globals.Session.User.Email      = res.User.Email;
          Globals.Session.User.Source     = res.User.Source;
          Globals.Session.User.RoleCode   = res.User.RoleCode;
          Globals.Session.User.RoleName   = res.User.RoleName;
          
          Globals.Session.User.CompanyId  = res.User.CompanyId;
          Globals.Session.User.CompanyCode= res.User.CompanyCode;
          Globals.Session.User.CompanyName= res.User.CompanyName;

          Globals.Session.User.BranchId   = res.User.BranchId;
          Globals.Session.User.BranchCode = res.User.BranchCode;
          Globals.Session.User.BranchName = res.User.BranchName;
          
          this.storage.set(Globals.QueueSession, {
                                                  User: res.User,
                                                  TokenId: res.TokenId, 
                                                  IsAuthenticated: res.IsAuthenticated
                                                  });
          loader.dismiss();
          this.events.publish("user:login");
          /*
          switch(res.User.RoleCode) { 
            case "FDO": { 
              this.navCtrl.navigateRoot(['/queue-list']);
              break; 
            } 
            case "CSA": { 
              this.navCtrl.navigateRoot(['/queue-call']);  
              break; 
            } 
            default: { 
              this.navCtrl.navigateRoot(['/edit-profile']);  
              break; 
            } 
          }*/
        } else{
          loader.dismiss();
          this.showError("Unable to gain access for user" + this.username + ".");
        }
    },err=>{
      loader.dismiss();
      this.showError(err);
    });
  }

  async showError(err:any){
    var errorText:string;

    if (Globals.isObject(err)){
      if (Globals.isObject(err.error)) {
        errorText="Unable to connect to server.";
      }else{
        errorText = err.error;
      }
    }else{
      errorText = err;
    }

    const toast = await this.toastCtrl.create({
      message : errorText,
      duration: 2000,
      color   : "danger"
    });
    toast.present();
  }
}

import { Component, OnInit} from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController, ModalController, AlertController } from '@ionic/angular';
import {QueueService} from '../../services/queue.service';

import { ImagePage } from '../modal/image/image.page';
import { Globals } from 'src/app/services/globals.service';
import { Queue,Param,VehInfo } from 'src/app/interfaces/queue';

@Component({
  selector: 'queue-new',
  templateUrl: './queue-call-edit.page.html',
  styleUrls: ['./queue-call-edit.page.scss'],
})
export class QueueCallEditPage implements OnInit {
  //callerroute:string;
  //itemId:number;
  grouptype:string;
  services:any;  
  item:Queue;
  vehinfo:any;
  imgpath:string;
  blockQueue:Boolean = true;
  comparewith:any;

  //test
  srvtypename:string;

  constructor(
      public navCtrl: NavController,
      public loadingCtrl: LoadingController,
      public toastCtrl: ToastController,
      public modalCtrl:ModalController,
      public qservice:QueueService,
      private route:ActivatedRoute,
      private router: Router,
      public alertCtrl:AlertController
  ){
    this.imgpath = Globals.EndPoint;
    this.item = this.qservice.getQueueTemplate();
    this.vehinfo  = new VehInfo();
    this.services = Globals.ServiceTypes;
    this.route.queryParams.subscribe((res) => {
      //this.callerroute = res.CallerRoute;
      this.grouptype = res.type;

      if (Globals.current_queue!=null){
        this.item = Globals.current_queue;
      }
      else{
        this.qservice.getNextQueueItem(this.grouptype).subscribe(
          (item:any)=>{
              this.item = item[0];
              console.log(this.item);
          },(err)=>{
            console.log(err);
            this.showError(err);
          });
      }

      this.setServiceTypeName();
     });
    //this.itemId = Number(this.route.snapshot.paramMap.get('id'));
    //console.log(this.services);
    
  }
  
  selectService($event:any){
    console.log($event.detail.value,this.item.ServiceTypeId);
    var srv = this.services.find(x => x.Id == this.item.ServiceTypeId);
    if (srv!=null){      
      this.srvtypename = srv.TypeName;
    }
        
   // console.log("this.srvtypename",this.srvtypename);
    

    //this.item.ServiceTypeId = $event.detail.value;
    //this.setServiceTypeName();
    //console.log(this.sel_service);
    /*
    if (this.sel_service){
      var srv = this.services.find(x => x.Id == this.sel_service);    
      this.sel_service = srv?srv.TypeName:null;    
    } 
    */   
  }

  setServiceTypeName(){    
    if (this.item.ServiceTypeId==null){
      this.item.ServiceTypeId = this.services[0].Id;
    }

    var srv = this.services.find(x => x.Id == this.item.ServiceTypeId);
    this.srvtypename = srv.TypeName;
    
  }
  ngOnInit() {
    
  }

  ionViewWillEnter(){
    
  }

  async validateEntries(){
    var ok :Boolean = false;
    //console.log(this.item.regno);

    if (this.item.RegNo){
      ok=false;
    }

    if (!ok){
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        //cssClass: 'bg-profile',
        message: 'Some of required fields are missing.',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
    
    return ok;
  }

  async sendData() {
   var ok = this.validateEntries();

   if (!ok) return;
    const loader = await this.loadingCtrl.create({
      duration: 2000
    });

    await loader.present();
    loader.onWillDismiss().then(async l => {
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        cssClass: 'bg-profile',
        message: 'Your profile was was edited!',
        duration: 3000,
        position: 'bottom'
      });

      toast.present();
      this.navCtrl.navigateForward('/queue-list');
    });
  }

  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  editCSA(){
    Globals.current_queue = this.item;
    //this.navCtrl.navigateForward('/csa-list');
    //this.router.navigate(['/csa-list'], {
      //queryParams: {CallerRoute:this.callerroute ,Id:this.itemId}
    //});
  }

  customPopoverOptions: any = {
    header: 'Service Type',
    //subHeader: 'Select Appointment Type',
    message: 'State the purpose of your visit'
  };

  closeModal() {
    Globals.current_queue = null;
    this.navCtrl.navigateBack("/queue-call");
  }

  async confirm(type:string){
    
    switch(type){
      case "Keydrop":{
                      //this.item.IsKeyDrop=true;
                      this.confirmAction("Keydrop",
                        "Receive car key?",
                        {type:type});
                      break;
                     }
      case "TestDrive":{
                        //this.item.IsTestDrive=true;
                        this.confirmAction("Test Drive",
                          "Mark this queue as test drive?",
                          {type:type});
                        break;
                      }
      case "Delete":{
                      //this.item.IsDeleted=true;
                      this.confirmAction("Delete Queue",
                        "Do you really want to delete this queue?",
                        {type:type});
                      break;
                    }
      case "Complete":{
                       //this.item.Status="Completed";
                       this.confirmAction("Confirm Complete",
                        "Finish serving customer?",
                        {type:type});
                       break;
                      }
      case "On-Hold":{
                       //this.item.Status="On-Hold";
                       this.confirmAction("Confirm to Put On-Hold",
                        "Put this queue on-hold?",
                        {type:type});
                       break;
                      }                          
      default:{
            this.confirmAction("Confirm Save",
                              "Save/Update queue details?",
                              {type:type});
            break;                              
      }                      
    }
  }

  async showError(err:any){
    var errorText;

    if (Globals.isObject(err.error)) {
      errorText="Unable to connect to server.";
    }else{
      errorText = err.error;
    }
  
    console.log(errorText);
    const toast = await this.toastCtrl.create({
      message : errorText,
      duration: 2000,
      color   : "danger"
    });
    toast.present();
  }

  compareWithFn(o1, o2) {
    console.log(o1,o2);
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  };


  checkValue(event:any){
    //console.log(event.detail.value,this.item.ServiceTypeId);
    this.item.ServiceTypeId = event.detail.value;
  }

  getVehicleInfo(){
    this.qservice.getVehicleInfo(this.item.RegNo,this.item.MobileNo).subscribe(
      (item:any)=>{  
          //console.log(item.length);      
          if (item.length>0){
            this.vehinfo = item[0];          
          }else{
            this.vehinfo = new VehInfo();
          }
          //this.item.CustomerName  = this.vehinfo.CustomerName;
          //this.item.ServiceDate   = this.vehinfo.ApptDate;
          //this.item.CSAUserId     = this.vehinfo.CSAUserId;
          //this.item.CSAName       = this.vehinfo.CSAName;
      },(err)=>{
        this.showError(err);
      });
  }

  async confirmAction(title:string,message:string,option:any) {
    const alert = await this.alertCtrl.create({
      header: title,
      message:message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',          
        }, {
          text: 'Ok',
          handler: () => {
            this.saveData(option);
          }
        }
      ]
    });

    await alert.present();
  }

  async saveData(option:any){

    switch(option.type){
      case "Keydrop":{this.item.IsKeyDrop=true;break;}
      case "Testdrive":{this.item.IsTestDrive=true;break;}
      case "Delete":{this.item.IsDeleted=true;break;}
      case "Complete":{this.item.Status="Completed";break;}
      case "On-Hold":{this.item.Status="On-Hold";break;}
    }

    const loader = await this.loadingCtrl.create({
      message: "Saving Changes..."
    });

    await loader.present();

    this.qservice.setQueueItem(this.item).subscribe(
      (res:any)=>{
        loader.dismiss();
        Globals.current_queue = null;
        this.navCtrl.navigateBack("/queue-call");
      },(err)=>{
        loader.dismiss();
        this.showError(err.error);        
      });
  }
}


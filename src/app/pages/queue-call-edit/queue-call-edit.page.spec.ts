import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueueCallEditPage } from './queue-call-edit.page';

describe('QueueEditPage', () => {
  let component: QueueCallEditPage;
  let fixture: ComponentFixture<QueueCallEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueCallEditPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueCallEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

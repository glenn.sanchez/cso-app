import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController, AlertController } from '@ionic/angular';
import { AppSession } from 'src/app/interfaces/appsession';
import { Globals } from 'src/app/services/globals.service';
import { AuthService } from 'src/app/services/auth.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  private session:AppSession;
  private oldpassword:string;
  private newpassword:string;
  private cnfpassword:string;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl:AlertController,
    public toastCtrl: ToastController,
    public auth:AuthService,
    public storage:Storage
    ) {       
      this.session = Globals.Session;
    }

  ngOnInit() {
  }

  async sendData() {
    const loader = await this.loadingCtrl.create({
      duration: 2000
    });

    await loader.present();
    loader.onWillDismiss().then(async l => {
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        cssClass: 'bg-profile',
        message: 'Your profile was was edited!',
        duration: 3000,
        position: 'bottom'
      });

      toast.present();
      this.navCtrl.navigateForward('/dashboard');
    });
  }

  setPassword(){

    if (this.newpassword!=this.cnfpassword && this.newpassword){
      this.showError("Password does not match.");
      return;
    }

    this.auth.setPassword(this.oldpassword,this.newpassword).subscribe((res)=>{
      this.showSuccess("Password has been changed successfully!");
    },err=>{
      this.showError(err);
    });
  }  

  async showSuccess(message:string) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2000
    });

    toast.present();  
  }

  async showError(err:any){
    var errorText:string;

    if (Globals.isObject(err)){
      if (Globals.isObject(err.error)) {
        errorText="Unable to connect to server.";
      }else{
        errorText = err.error;
      }
    }else{
      errorText = err;
    }
    
  
    const toast = await this.toastCtrl.create({
      message : errorText,
      duration: 2000,
      color   : "danger"
    });
    toast.present();
  }

  async logout() {
    //Logout Calling WebAPI

    const alert = await this.alertCtrl.create({
      header: 'Log-out Session',
      message:'Do you want to log out?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',          
        }, {
          text: 'Yes',
          handler: () => {
            this.endSession();
          }
        }
      ]
    });

    await alert.present();

    
  }


  async endSession(){

    const loader = await this.loadingCtrl.create({
      message: "Ending session..."
    });

    await loader.present();    

    this.auth.logout().subscribe(res=>{    
      console.log(res);
        this.storage.remove(Globals.QueueSession).then(()=>{      
          loader.dismiss();
          this.navCtrl.navigateRoot('/');
        });    
    },error=>{
      loader.dismiss();
      this.showError(error);
    });
  }
}

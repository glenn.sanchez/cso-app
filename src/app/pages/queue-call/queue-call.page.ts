import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { QueueService } from 'src/app/services/queue.service';
import { Globals } from 'src/app/services/globals.service';
import { ToastController, MenuController, NavController, LoadingController } from '@ionic/angular';

class SumItem{
  GroupCode: string;
  GroupName: string;
  GroupDesc: string;
  Counts: number;
}

@Component({
  selector: 'queue-call',
  templateUrl: './queue-call.page.html',
  styleUrls: ['./queue-call.page.scss'],
})

export class QueueCallPage  {
  mec:SumItem;
  bnp:SumItem;
  col:SumItem;
  hld:SumItem;
  tow:SumItem;
  srv:SumItem;
  srvcol:SumItem;
  cmp:SumItem;
  sumry:SumItem[];
  user:any;

  constructor(public navCtrl: NavController,
              private router: Router,
              public qservice:QueueService,
              public toastCtrl:ToastController,
              public menuCtrl: MenuController,
              public loadingCtrl:LoadingController) {

    this.srv = new SumItem();
    this.mec = new SumItem();
    this.bnp = new SumItem();
    this.col = new SumItem();
    this.hld = new SumItem();
    this.tow = new SumItem();
    this.cmp = new SumItem();
    this.srvcol = new SumItem();
    this.user = Globals.Session.User;
   }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.menuCtrl.enable(true);

    this.loadData();
   }

  async loadData(){
    const loader = await this.loadingCtrl.create({
      message: "Loading..."
    });
    await loader.present();

    this.qservice.getQueueSummary().subscribe(
      (items:any)=>{
          loader.dismiss();
          this.mec = items.find(x => x.GroupCode == 'MEC');
          this.bnp = items.find(x => x.GroupCode == 'BNP');
          this.col = items.find(x => x.GroupCode == 'COL');
          this.hld = items.find(x => x.GroupCode == 'HLD');
          this.srv = items.find(x => x.GroupCode == 'SRV');
          this.tow = items.find(x => x.GroupCode == 'TOW');
          this.cmp = items.find(x => x.GroupCode == 'CMP');
          this.srvcol = items.find(x => x.GroupCode == 'SRVCOL');
      },(err)=>{
        loader.dismiss();
        this.showError(err);
      });
  }

  nextQueue(type:string){
    this.router.navigate(['/queue-call-edit'], {
      queryParams: {type:type}
    });
  }

  async showError(err:any){
    var errorText;

    if (Globals.isObject(err.error)) {
      errorText="Unable to connect to server.";
    }else{
      errorText = err.error;
    }
  
    const toast = await this.toastCtrl.create({
      message : errorText,
      duration: 2000,
      color   : "danger"
    });
    toast.present();
  }

  viewSumItems(groupcd:string,statuscd:string){
    this.router.navigate(['/queue-list'], {
      queryParams: {
                    groupcd:groupcd,
                    statuscd:statuscd,
                    rootcaller:'/queue-call'
                   }
    });
  }

  reRouteTo(route:string) {
    this.navCtrl.navigateForward(route);
  }
}

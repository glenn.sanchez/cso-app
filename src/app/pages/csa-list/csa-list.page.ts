import { Component, ɵConsole } from '@angular/core';
import {NavController,MenuController,  
        ModalController,LoadingController, ToastController} from '@ionic/angular';
import { Globals } from 'src/app/services/globals.service';
import {QueueService} from '../../services/queue.service';
import { Router,ActivatedRoute } from '@angular/router';


class Param{
  rootcaller: string;
  caller:string;
  groupcd:string;
  statuscd:string;
  csauserid:number;
}

@Component({
  selector: 'csa-list',
  templateUrl: './csa-list.page.html',
  styleUrls: ['./csa-list.page.scss']
})
export class CSAListPage {
  searchKey = '';
  csauserid = null;
  item = null;
  items = [];
  allitems = [];
  params:any = null;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public qservice:QueueService,
    public loadingCtrl:LoadingController,
    private route:ActivatedRoute,
    private router: Router,
    private toastCtrl:ToastController
  ) {
    this.route.queryParams.subscribe((res) => {
      this.params = res;      
    },err=>{
      this.showError(err);
    });
  }

  ngOnInit() {
    
  }
  
  ionViewWillEnter(){
    this.csauserid = this.params.csauserid;
    this.loadData();
  }
  async loadData() {
    const loading = await this.loadingCtrl.create({
      message: 'Loading...'
    });

    await loading.present();

    this.qservice.getCSAItems().subscribe(
      (items:any[])=>{
          this.items = items;
          this.allitems = items;          
          console.log(items);
          loading.dismiss();
      });
  }

  async select(){

    //console.log("this.csauserid",this.csauserid);
    Globals.current_queue.CSAUserId = this.csauserid;
    //console.log(Globals.current_queue);

    if (this.csauserid){
      var csa = this.items.find(x => x.UserId == this.csauserid);              
      Globals.current_queue.CSAName = csa.DisplayName;
    }

    if (Globals.current_queue.Id){
      const toast = await this.toastCtrl.create({
        message : "CSA assignment has been updated.",
        duration: 2000,
        color   : "success"
      });

      this.qservice.setQueueItem(Globals.current_queue).subscribe(
      (res:any)=>{
        console.log(res);
        toast.present();    

        this.navCtrl.setDirection('back');
        this.router.navigate(['/queue-edit'], {
          queryParams: this.params    
        });
      },(err)=>{
        console.log(err);
        this.showError(err);
      });    
    }else{
      this.router.navigate(['/queue-edit'], {
        queryParams: this.params    
      });
    }
  }

  close(){
    this.navCtrl.setDirection("back");
    this.router.navigate(['/queue-edit'], {
      queryParams: this.params    
    });

    //this.navCtrl.navigateBack("/queue-edit");
  }

  selectChange(userId:number){

    console.log(userId==this.csauserid);

    if (userId==this.csauserid){
      this.csauserid = null;      
    }else{
      this.csauserid = userId;
    }

    this.item = this.items.find(x => x.UserId == this.csauserid);
  }

  async showError(err:any){
    var errorText;
    
    if (Globals.isObject(err.error)) {
      errorText="Unable to connect to server.";
    }else{
      errorText = err.error;
    }
  
    const toast = await this.toastCtrl.create({
      message : errorText,
      duration: 2000,
      color   : "danger"
    });
    toast.present();
  }

  filterData(){
    var newArray;

    if (this.searchKey){
      newArray = this.allitems.filter(el=> el.DisplayName.toLowerCase().indexOf(this.searchKey.toLowerCase()) > -1);
    }else{
      newArray = this.allitems;
    }
     
    this.items = newArray;
  }
}

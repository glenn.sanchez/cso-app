import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CSAListPage } from './csa-list.page';

describe('CSAListPage', () => {
  let component: CSAListPage;
  let fixture: ComponentFixture<CSAListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CSAListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CSAListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

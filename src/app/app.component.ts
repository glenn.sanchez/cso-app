import { Component } from '@angular/core';

import { Platform, NavController,Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Pages } from './interfaces/pages';
import { Globals } from './services/globals.service';
import { AppSession } from './interfaces/appsession';
import { Storage } from '@ionic/storage';
import { ConfigService } from './services/config.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public Session:AppSession;
  public appPages: Array<Pages>;
  
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    public events:Events,
    private storage: Storage,
    public cfgservice:ConfigService
  ) {
    /*
    this.appPages = [
      //{title: 'Customers',url: '/queue-list',direct: 'root',icon: 'people'},
      {title: 'Test Drives',url: '/testdrive-list',direct: 'forward',icon: 'car'},
      {title: 'Queue Calling',url: '/queue-call',direct: 'root',icon: 'mic'},
      {title: 'About',url: '/about',direct: 'forward',icon: 'information-circle-outline'},
      {title: 'App Settings',url: '/settings',direct: 'forward',icon: 'cog'}
    ];
    */

    events.subscribe('user:login', () => {

      this.setApp();
      
    });


    this.initializeApp();
  }

  initializeApp() {
    Globals.Session = new AppSession();
    this.Session = Globals.Session;

    
    this.platform.ready().then(() => {
      
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.storage.get(Globals.QueueSession).then((val)=>{        
        if (val!=null){
          Globals.Session.User = val.User;
          Globals.Session.TokenId = val.TokenId;
          Globals.Session.IsAuthenticated = val.IsAuthenticated;
          this.setApp();
        }
      });

    }).catch(() => {});
  }

  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  logout() {
    //Logout Calling WebAPI

    this.storage.remove(Globals.QueueSession).then(()=>{      
      this.navCtrl.navigateRoot('/');
    });
  }

  setApp(){
    this.Session = Globals.Session;
      this.appPages = [];      

      this.cfgservice.initSetup();
      /*
      if (this.Session.User.RoleCode=="FDO"){
        this.appPages.push({title: 'Customers',url: '/queue-list',direct: 'root',icon: 'people'});
        this.appPages.push({title: 'Test Drives',url: '/testdrive-list',direct: 'forward',icon: 'car'});
      }
      if (this.Session.User.RoleCode== "CSA"){
        this.appPages.push({title: 'Queue Calling',url: '/queue-call',direct: 'root',icon: 'mic'});
      }    
      this.appPages.push({title: 'App Settings',url: '/settings',direct: 'forward',icon: 'cog'});
      */
      this.navCtrl.navigateRoot(['/queue-call']);

      /*
      switch(this.Session.User.RoleCode) { 
        case "FDO": { 
          this.navCtrl.navigateRoot(['/queue-list']);
          break; 
        } 
        case "CSA": { 
          this.navCtrl.navigateRoot(['/queue-call']);  
          break; 
        } 
        default: { 
          this.navCtrl.navigateRoot(['/edit-profile']);  
          break; 
        } 
      }*/

  }
}

import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
//import { QueueEditPage } from '../../pages/queue-edit/queue-edit.page';
var PopmenuComponent = /** @class */ (function () {
    function PopmenuComponent(navCtrl, modCtrl) {
        this.navCtrl = navCtrl;
        this.modCtrl = modCtrl;
        this.openMenu = false;
    }
    PopmenuComponent.prototype.ngOnInit = function () {
    };
    PopmenuComponent.prototype.togglePopupMenu = function () {
        return this.openMenu = !this.openMenu;
    };
    PopmenuComponent.prototype.goToQueueList = function () {
        var _this = this;
        this.navCtrl.navigateForward('queue-list').finally(function () {
            _this.openMenu = false;
        });
    };
    PopmenuComponent.prototype.goToQueueAdd = function () {
        var _this = this;
        this.navCtrl.navigateForward('queue-new').finally(function () {
            _this.openMenu = false;
        });
    };
    PopmenuComponent.prototype.addItem = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    PopmenuComponent = tslib_1.__decorate([
        Component({
            selector: 'popmenu',
            templateUrl: './popmenu.component.html',
            styleUrls: ['./popmenu.component.scss']
        })
        //modal
        ,
        tslib_1.__metadata("design:paramtypes", [NavController,
            ModalController])
    ], PopmenuComponent);
    return PopmenuComponent;
}());
export { PopmenuComponent };
//# sourceMappingURL=popmenu.component.js.map
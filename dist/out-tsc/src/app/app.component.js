import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform, NavController, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Globals } from './services/globals.service';
import { AppSession } from './interfaces/appsession';
import { Storage } from '@ionic/storage';
import { ConfigService } from './services/config.service';
var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar, navCtrl, events, storage, cfgservice) {
        /*
        this.appPages = [
          //{title: 'Customers',url: '/queue-list',direct: 'root',icon: 'people'},
          {title: 'Test Drives',url: '/testdrive-list',direct: 'forward',icon: 'car'},
          {title: 'Queue Calling',url: '/queue-call',direct: 'root',icon: 'mic'},
          {title: 'About',url: '/about',direct: 'forward',icon: 'information-circle-outline'},
          {title: 'App Settings',url: '/settings',direct: 'forward',icon: 'cog'}
        ];
        */
        var _this = this;
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.navCtrl = navCtrl;
        this.events = events;
        this.storage = storage;
        this.cfgservice = cfgservice;
        events.subscribe('user:login', function () {
            _this.setApp();
        });
        this.initializeApp();
    }
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        Globals.Session = new AppSession();
        this.Session = Globals.Session;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.storage.get(Globals.QueueSession).then(function (val) {
                if (val != null) {
                    Globals.Session.User = val.User;
                    Globals.Session.TokenId = val.TokenId;
                    Globals.Session.IsAuthenticated = val.IsAuthenticated;
                    _this.setApp();
                }
            });
        }).catch(function () { });
    };
    AppComponent.prototype.goToEditProgile = function () {
        this.navCtrl.navigateForward('edit-profile');
    };
    AppComponent.prototype.logout = function () {
        //Logout Calling WebAPI
        var _this = this;
        this.storage.remove(Globals.QueueSession).then(function () {
            _this.navCtrl.navigateRoot('/');
        });
    };
    AppComponent.prototype.setApp = function () {
        this.Session = Globals.Session;
        this.appPages = [];
        this.cfgservice.initSetup();
        if (this.Session.User.RoleCode == "FDO") {
            this.appPages.push({ title: 'Customers', url: '/queue-list', direct: 'root', icon: 'people' });
            this.appPages.push({ title: 'Test Drives', url: '/testdrive-list', direct: 'forward', icon: 'car' });
        }
        if (this.Session.User.RoleCode == "CSA") {
            this.appPages.push({ title: 'Queue Calling', url: '/queue-call', direct: 'root', icon: 'mic' });
        }
        this.appPages.push({ title: 'App Settings', url: '/settings', direct: 'forward', icon: 'cog' });
        switch (this.Session.User.RoleCode) {
            case "FDO": {
                this.navCtrl.navigateRoot(['/queue-list']);
                break;
            }
            case "CSA": {
                this.navCtrl.navigateRoot(['/queue-call']);
                break;
            }
            default: {
                this.navCtrl.navigateRoot(['/edit-profile']);
                break;
            }
        }
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html',
            styleUrls: ['./app.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            SplashScreen,
            StatusBar,
            NavController,
            Events,
            Storage,
            ConfigService])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map
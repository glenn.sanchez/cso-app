import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, MenuController, LoadingController } from '@ionic/angular';
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, menuCtrl, loadingCtrl, formBuilder) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
    }
    RegisterPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    RegisterPage.prototype.ngOnInit = function () {
        this.onRegisterForm = this.formBuilder.group({
            'fullName': [null, Validators.compose([
                    Validators.required
                ])],
            'email': [null, Validators.compose([
                    Validators.required
                ])],
            'password': [null, Validators.compose([
                    Validators.required
                ])]
        });
    };
    RegisterPage.prototype.signUp = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loader;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            duration: 2000
                        })];
                    case 1:
                        loader = _a.sent();
                        loader.present();
                        loader.onWillDismiss().then(function () {
                            _this.navCtrl.navigateRoot('/home-results');
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    // // //
    RegisterPage.prototype.goToLogin = function () {
        this.navCtrl.navigateRoot('/');
    };
    RegisterPage = tslib_1.__decorate([
        Component({
            selector: 'app-register',
            templateUrl: './register.page.html',
            styleUrls: ['./register.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            LoadingController,
            FormBuilder])
    ], RegisterPage);
    return RegisterPage;
}());
export { RegisterPage };
//# sourceMappingURL=register.page.js.map
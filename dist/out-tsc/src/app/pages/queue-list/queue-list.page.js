import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController, MenuController, ToastController, PopoverController, LoadingController, ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
// Modals
import { SearchFilterPage } from '../modal/search-filter/search-filter.page';
import { NotifListPage } from '../modal/notif-list/notif-list.page';
import { ImagePage } from '../modal/image/image.page';
import { QueueService } from '../../services/queue.service';
import { Globals } from 'src/app/services/globals.service';
var Param = /** @class */ (function () {
    function Param() {
    }
    return Param;
}());
var QueueListPage = /** @class */ (function () {
    //statuscd:string;
    function QueueListPage(navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, qservice, loadingCtrl, router, route, toastCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.qservice = qservice;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.toastCtrl = toastCtrl;
        this.searchKey = '';
        this.items = [];
        this.params = new Param();
        this.imgpath = Globals.EndPoint;
        console.log('params', this.params);
    }
    QueueListPage.prototype.ngOnInit = function () {
    };
    QueueListPage.prototype.loadData = function (groupcd, statuscd) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Loading...',
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.qservice.getQueueItems(false, null, groupcd, statuscd).subscribe(function (items) {
                            loading.dismiss();
                            console.log(items);
                            _this.items = items;
                        }, function (err) {
                            loading.dismiss();
                            _this.showError(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    QueueListPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.menuCtrl.enable(true);
        this.items = null;
        this.route.queryParams.subscribe(function (res) {
            _this.params.rootcaller = res.rootcaller;
            _this.params.groupcd = res.groupcd;
            _this.params.statuscd = res.statuscd;
            _this.loadData(_this.params.groupcd, _this.params.statuscd);
        }, function (err) {
            console.log("no error");
        });
    };
    QueueListPage.prototype.settings = function () {
        this.navCtrl.navigateForward('settings');
    };
    QueueListPage.prototype.searchFilter = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: SearchFilterPage
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    QueueListPage.prototype.viewItem = function (itemId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                //this.navCtrl.navigateForward('queue-edit/' + itemId);
                this.router.navigate(['/queue-edit'], {
                    queryParams: {
                        rootcaller: this.params.rootcaller,
                        caller: '/queue-list',
                        id: itemId,
                        groupcd: this.params.groupcd,
                        statuscd: this.params.statuscd
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    QueueListPage.prototype.newItem = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                //this.navCtrl.navigateForward('queue-edit/');
                this.router.navigate(['/queue-edit'], {
                    queryParams: {
                        rootcaller: this.params.rootcaller,
                        caller: '/queue-list',
                        id: null,
                        groupcd: this.params.groupcd,
                        statuscd: this.params.statuscd
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    QueueListPage.prototype.closeModal = function () {
        this.navCtrl.setDirection('back');
        this.router.navigate([this.params.rootcaller], {
            queryParams: this.params
        });
    };
    QueueListPage.prototype.showNotifications = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: NotifListPage
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    QueueListPage.prototype.presentImage = function (image) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: ImagePage,
                            componentProps: { value: image }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    QueueListPage.prototype.showError = function (err) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var errorText, toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Globals.isObject(err.error)) {
                            errorText = "Unable to connect to server.";
                        }
                        else {
                            errorText = err.error;
                        }
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: errorText,
                                duration: 2000,
                                color: "danger"
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    QueueListPage.prototype.goToProfile = function () {
        this.navCtrl.navigateForward('edit-profile');
    };
    QueueListPage = tslib_1.__decorate([
        Component({
            selector: 'queue-list',
            templateUrl: './queue-list.page.html',
            styleUrls: ['./queue-list.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            PopoverController,
            AlertController,
            ModalController,
            QueueService,
            LoadingController,
            Router,
            ActivatedRoute,
            ToastController])
    ], QueueListPage);
    return QueueListPage;
}());
export { QueueListPage };
//# sourceMappingURL=queue-list.page.js.map
import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { QueueListPage } from './queue-list.page';
var routes = [
    {
        path: '',
        component: QueueListPage
    }
];
var QueueListPageModule = /** @class */ (function () {
    function QueueListPageModule() {
    }
    QueueListPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [QueueListPage]
        })
    ], QueueListPageModule);
    return QueueListPageModule;
}());
export { QueueListPageModule };
//# sourceMappingURL=queue-list.module.js.map
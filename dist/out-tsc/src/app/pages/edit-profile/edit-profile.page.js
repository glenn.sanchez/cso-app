import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Globals } from 'src/app/services/globals.service';
import { AuthService } from 'src/app/services/auth.service';
import { Storage } from '@ionic/storage';
var EditProfilePage = /** @class */ (function () {
    function EditProfilePage(navCtrl, loadingCtrl, toastCtrl, auth, storage) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.auth = auth;
        this.storage = storage;
        this.session = Globals.Session;
    }
    EditProfilePage.prototype.ngOnInit = function () {
    };
    EditProfilePage.prototype.sendData = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loader;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            duration: 2000
                        })];
                    case 1:
                        loader = _a.sent();
                        loader.present();
                        loader.onWillDismiss().then(function (l) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var toast;
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                                            showCloseButton: true,
                                            cssClass: 'bg-profile',
                                            message: 'Your profile was was edited!',
                                            duration: 3000,
                                            position: 'bottom'
                                        })];
                                    case 1:
                                        toast = _a.sent();
                                        toast.present();
                                        this.navCtrl.navigateForward('/dashboard');
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    EditProfilePage.prototype.setPassword = function () {
        var _this = this;
        if (this.newpassword != this.cnfpassword && this.newpassword) {
            this.showError("Password does not match.");
            return;
        }
        this.auth.setPassword(this.oldpassword, this.newpassword).subscribe(function (res) {
            _this.showSuccess("Password has been changed successfully!");
        }, function (err) {
            _this.showError(err);
        });
    };
    EditProfilePage.prototype.showSuccess = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: message,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    EditProfilePage.prototype.showError = function (err) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var errorText, toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Globals.isObject(err)) {
                            if (Globals.isObject(err.error)) {
                                errorText = "Unable to connect to server.";
                            }
                            else {
                                errorText = err.error;
                            }
                        }
                        else {
                            errorText = err;
                        }
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: errorText,
                                duration: 2000,
                                color: "danger"
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    EditProfilePage.prototype.logout = function () {
        //Logout Calling WebAPI
        var _this = this;
        this.storage.remove(Globals.QueueSession).then(function () {
            _this.navCtrl.navigateRoot('/');
        });
    };
    EditProfilePage = tslib_1.__decorate([
        Component({
            selector: 'app-edit-profile',
            templateUrl: './edit-profile.page.html',
            styleUrls: ['./edit-profile.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            LoadingController,
            ToastController,
            AuthService,
            Storage])
    ], EditProfilePage);
    return EditProfilePage;
}());
export { EditProfilePage };
//# sourceMappingURL=edit-profile.page.js.map
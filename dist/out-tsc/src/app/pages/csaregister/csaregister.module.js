import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CSARegisterPage } from './csaregister.page';
var routes = [
    {
        path: '',
        component: CSARegisterPage
    }
];
var CSARegisterPageModule = /** @class */ (function () {
    function CSARegisterPageModule() {
    }
    CSARegisterPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CSARegisterPage]
        })
    ], CSARegisterPageModule);
    return CSARegisterPageModule;
}());
export { CSARegisterPageModule };
//# sourceMappingURL=csaregister.module.js.map
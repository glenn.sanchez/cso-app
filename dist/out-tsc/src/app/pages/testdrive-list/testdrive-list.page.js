import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController, MenuController, ToastController, PopoverController, LoadingController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
// Modals
import { SearchFilterPage } from '../modal/search-filter/search-filter.page';
import { NotifListPage } from '../modal/notif-list/notif-list.page';
import { ImagePage } from '../modal/image/image.page';
import { QueueService } from '../../services/queue.service';
import { Globals } from 'src/app/services/globals.service';
//import { QueueEditPage } from '../queue-edit/queue-edit.page';
//import { QueueNewPage } from '../queue-new/queue-new.page';
var TestDriveListPage = /** @class */ (function () {
    function TestDriveListPage(navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, toastCtrl, qservice, loadingCtrl, router) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.qservice = qservice;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.searchKey = '';
        this.items = [];
    }
    TestDriveListPage.prototype.ngOnInit = function () {
    };
    TestDriveListPage.prototype.loadData = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Loading...',
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.qservice.getQueueItems(true, null, null, null).subscribe(function (items) {
                            _this.items = items;
                            //console.log(this.items);
                            loading.dismiss();
                        }, function (err) {
                            _this.showError(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    TestDriveListPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
        this.loadData();
    };
    TestDriveListPage.prototype.settings = function () {
        this.navCtrl.navigateForward('settings');
    };
    TestDriveListPage.prototype.searchFilter = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: SearchFilterPage
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TestDriveListPage.prototype.viewItem = function (itemId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                //this.navCtrl.navigateForward('queue-edit/' + itemId);
                console.log(itemId);
                this.router.navigate(['/queue-edit'], {
                    queryParams: {
                        caller: '/testdrive-list',
                        id: itemId
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    TestDriveListPage.prototype.newItem = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                //this.navCtrl.navigateForward('queue-edit/');
                this.router.navigate(['/queue-edit'], {
                    queryParams: {
                        caller: '/testdrive-list',
                        id: null
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    TestDriveListPage.prototype.showNotifications = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: NotifListPage
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TestDriveListPage.prototype.presentImage = function (image) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: ImagePage,
                            componentProps: { value: image }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TestDriveListPage.prototype.showError = function (err) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var errorText, toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Globals.isObject(err.error)) {
                            errorText = "Unable to connect to server.";
                        }
                        else {
                            errorText = err.error;
                        }
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: errorText,
                                duration: 2000,
                                color: "danger"
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    TestDriveListPage = tslib_1.__decorate([
        Component({
            selector: 'app-list',
            templateUrl: './testdrive-list.page.html',
            styleUrls: ['./testdrive-list.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            PopoverController,
            AlertController,
            ModalController,
            ToastController,
            QueueService,
            LoadingController,
            Router])
    ], TestDriveListPage);
    return TestDriveListPage;
}());
export { TestDriveListPage };
//# sourceMappingURL=testdrive-list.page.js.map
import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TestDriveListPage } from './testdrive-list.page';
var routes = [
    {
        path: '',
        component: TestDriveListPage
    }
];
var TestDriveListPageModule = /** @class */ (function () {
    function TestDriveListPageModule() {
    }
    TestDriveListPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TestDriveListPage]
        })
    ], TestDriveListPageModule);
    return TestDriveListPageModule;
}());
export { TestDriveListPageModule };
//# sourceMappingURL=testdrive-list.module.js.map
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController, MenuController, ToastController, ModalController, LoadingController } from '@ionic/angular';
import { NotifListPage } from '../modal/notif-list/notif-list.page';
var DashboardPage = /** @class */ (function () {
    function DashboardPage(navCtrl, menuCtrl, alertCtrl, modalCtrl, toastCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    DashboardPage.prototype.ngOnInit = function () {
    };
    DashboardPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
    };
    DashboardPage.prototype.settings = function () {
        this.navCtrl.navigateForward('settings');
    };
    DashboardPage.prototype.showNotifications = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: NotifListPage
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DashboardPage = tslib_1.__decorate([
        Component({
            selector: 'app-dashboard',
            templateUrl: './dashboard.page.html',
            styleUrls: ['./dashboard.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            AlertController,
            ModalController,
            ToastController,
            LoadingController])
    ], DashboardPage);
    return DashboardPage;
}());
export { DashboardPage };
//# sourceMappingURL=dashboard.page.js.map
import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ImagePage } from './image.page';
var routes = [
    {
        path: '',
        component: ImagePage
    }
];
var ImagePageModule = /** @class */ (function () {
    function ImagePageModule() {
    }
    ImagePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ImagePage],
            entryComponents: [ImagePage]
        })
    ], ImagePageModule);
    return ImagePageModule;
}());
export { ImagePageModule };
//# sourceMappingURL=image.module.js.map
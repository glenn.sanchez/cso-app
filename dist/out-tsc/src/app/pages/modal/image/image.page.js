import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
var ImagePage = /** @class */ (function () {
    function ImagePage(nav, modalCtrl, sanitizer) {
        this.nav = nav;
        this.modalCtrl = modalCtrl;
        this.sanitizer = sanitizer;
    }
    ImagePage.prototype.ngOnInit = function () {
        this.image = this.sanitizer.bypassSecurityTrustStyle(this.value);
    };
    ImagePage.prototype.closeModal = function () {
        this.modalCtrl.dismiss();
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], ImagePage.prototype, "value", void 0);
    ImagePage = tslib_1.__decorate([
        Component({
            selector: 'app-image',
            templateUrl: './image.page.html',
            styleUrls: ['./image.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            ModalController,
            DomSanitizer])
    ], ImagePage);
    return ImagePage;
}());
export { ImagePage };
//# sourceMappingURL=image.page.js.map
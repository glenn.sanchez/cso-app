import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController, ModalController } from '@ionic/angular';
// Modals
//import { SearchFilterPage } from '../modal/search-filter/search-filter.page';
// Call notifications test by Popover and Custom Component.
var NotifListPage = /** @class */ (function () {
    function NotifListPage(navCtrl, menuCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.searchKey = '';
        this.items = [
            { id: 1, type: 'VEHARRIVAL', text: 'Vehicle with Reg.No WEWE772 as arrived.', contactname: 'Mr. Phang Yuan Chun', scheduledon: '30-Apr-2019 3:00 PM' },
            { id: 1, type: 'VEHARRIVAL', text: 'Vehicle with Reg.No TRS9388 as arrived.', contactname: 'Mr. Carpio Bernard Leyba', scheduledon: '30-Apr-2019 3:15 PM' },
            { id: 1, type: 'VEHARRIVAL', text: 'Vehicle with Reg.No TC 778 as arrived.', contactname: 'Ms. Nadia Athira Aznan', scheduledon: '30-Apr-2019 3:15 PM' },
            { id: 1, type: 'VEHARRIVAL', text: 'Vehicle with Reg.No JAW 9 as arrived.', contactname: 'Mr. Loo Chuan Wei', scheduledon: '30-Apr-2019 3:30 PM' },
            { id: 1, type: 'VEHARRIVAL', text: 'Vehicle with Reg.No GGS 100 as arrived.', contactname: 'Mr. Glenn Sanchez', scheduledon: '30-Apr-2019 3:45 PM' },
            { id: 1, type: 'VEHARRIVAL', text: 'Vehicle with Reg.No SDM 786 as arrived.', contactname: 'Mr. Tan Khim Hoe', scheduledon: '30-Apr-2019 4:00 PM' },
            { id: 1, type: 'VEHARRIVAL', text: 'Vehicle with Reg.No REP 980 as arrived.', contactname: 'Mr. Wong Michael', scheduledon: '30-Apr-2019 4:00 PM' },
            { id: 1, type: 'VEHARRIVAL', text: 'Vehicle with Reg.No VHP 0299 as arrived.', contactname: 'Ms. Nur Izzati Omar', scheduledon: '30-Apr-2019 4:00 PM' }
        ];
    }
    NotifListPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
    };
    NotifListPage.prototype.viewItem = function (itemId) {
        this.navCtrl.navigateForward('/notif-list');
    };
    NotifListPage.prototype.closeModal = function () {
        this.modalCtrl.dismiss();
    };
    NotifListPage = tslib_1.__decorate([
        Component({
            selector: 'notif-list',
            templateUrl: './notif-list.page.html',
            styleUrls: ['./notif-list.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            ModalController])
    ], NotifListPage);
    return NotifListPage;
}());
export { NotifListPage };
//# sourceMappingURL=notif-list.page.js.map
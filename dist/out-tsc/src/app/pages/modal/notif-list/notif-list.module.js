import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NotifListPage } from './notif-list.page';
var routes = [
    {
        path: '',
        component: NotifListPage
    }
];
var NotifListPageModule = /** @class */ (function () {
    function NotifListPageModule() {
    }
    NotifListPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [NotifListPage]
        })
    ], NotifListPageModule);
    return NotifListPageModule;
}());
export { NotifListPageModule };
//# sourceMappingURL=notif-list.module.js.map
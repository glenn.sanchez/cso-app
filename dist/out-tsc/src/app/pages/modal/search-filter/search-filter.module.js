import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SearchFilterPage } from './search-filter.page';
var routes = [
    {
        path: '',
        component: SearchFilterPage
    }
];
var SearchFilterPageModule = /** @class */ (function () {
    function SearchFilterPageModule() {
    }
    SearchFilterPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [SearchFilterPage]
        })
    ], SearchFilterPageModule);
    return SearchFilterPageModule;
}());
export { SearchFilterPageModule };
//# sourceMappingURL=search-filter.module.js.map
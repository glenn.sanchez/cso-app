import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
var SearchFilterPage = /** @class */ (function () {
    function SearchFilterPage(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.radiusmiles = 1;
        this.minmaxprice = {
            upper: 500,
            lower: 10
        };
    }
    SearchFilterPage.prototype.ngOnInit = function () {
    };
    SearchFilterPage.prototype.closeModal = function () {
        this.modalCtrl.dismiss();
    };
    SearchFilterPage = tslib_1.__decorate([
        Component({
            selector: 'app-search-filter',
            templateUrl: './search-filter.page.html',
            styleUrls: ['./search-filter.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ModalController])
    ], SearchFilterPage);
    return SearchFilterPage;
}());
export { SearchFilterPage };
//# sourceMappingURL=search-filter.page.js.map
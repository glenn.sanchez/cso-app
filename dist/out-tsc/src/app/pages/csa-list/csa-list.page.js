import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController, ModalController, LoadingController, ToastController } from '@ionic/angular';
import { Globals } from 'src/app/services/globals.service';
import { QueueService } from '../../services/queue.service';
import { Router, ActivatedRoute } from '@angular/router';
var Param = /** @class */ (function () {
    function Param() {
    }
    return Param;
}());
var CSAListPage = /** @class */ (function () {
    function CSAListPage(navCtrl, menuCtrl, modalCtrl, qservice, loadingCtrl, route, router, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.qservice = qservice;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.searchKey = '';
        this.csauserid = null;
        this.item = null;
        this.items = [];
        this.params = null;
        this.route.queryParams.subscribe(function (res) {
            _this.params = res;
        }, function (err) {
            _this.showError(err);
        });
    }
    CSAListPage.prototype.ngOnInit = function () {
    };
    CSAListPage.prototype.ionViewWillEnter = function () {
        this.csauserid = this.params.csauserid;
        this.loadData();
    };
    CSAListPage.prototype.loadData = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Loading...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.qservice.getCSAItems().subscribe(function (items) {
                            _this.items = items;
                            console.log(items);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    CSAListPage.prototype.select = function () {
        var _this = this;
        Globals.current_queue.CSAUserId = this.csauserid;
        if (this.csauserid) {
            var csa = this.items.find(function (x) { return x.UserId == _this.csauserid; });
            Globals.current_queue.CSAName = csa.DisplayName;
        }
        //this.navCtrl.navigateBack(this.params.caller);
        this.navCtrl.setDirection('back');
        this.router.navigate(['/queue-edit'], {
            queryParams: this.params
        });
        //this.navCtrl.navigateBack("/queue-edit");
    };
    CSAListPage.prototype.close = function () {
        this.navCtrl.setDirection("back");
        this.router.navigate(['/queue-edit'], {
            queryParams: this.params
        });
        //this.navCtrl.navigateBack("/queue-edit");
    };
    CSAListPage.prototype.selectChange = function (userId) {
        var _this = this;
        if (userId == this.csauserid) {
            this.csauserid = null;
        }
        else {
            this.csauserid = userId;
        }
        this.item = this.items.find(function (x) { return x.UserId == _this.csauserid; });
    };
    CSAListPage.prototype.showError = function (err) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var errorText, toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Globals.isObject(err.error)) {
                            errorText = "Unable to connect to server.";
                        }
                        else {
                            errorText = err.error;
                        }
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: errorText,
                                duration: 2000,
                                color: "danger"
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CSAListPage = tslib_1.__decorate([
        Component({
            selector: 'csa-list',
            templateUrl: './csa-list.page.html',
            styleUrls: ['./csa-list.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            ModalController,
            QueueService,
            LoadingController,
            ActivatedRoute,
            Router,
            ToastController])
    ], CSAListPage);
    return CSAListPage;
}());
export { CSAListPage };
//# sourceMappingURL=csa-list.page.js.map
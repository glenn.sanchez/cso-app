import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CSAListPage } from './csa-list.page';
var routes = [
    {
        path: '',
        component: CSAListPage
    }
];
var CSAListPageModule = /** @class */ (function () {
    function CSAListPageModule() {
    }
    CSAListPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CSAListPage]
        })
    ], CSAListPageModule);
    return CSAListPageModule;
}());
export { CSAListPageModule };
//# sourceMappingURL=csa-list.module.js.map
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController, Events } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Globals } from 'src/app/services/globals.service';
import { Storage } from '@ionic/storage';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, menuCtrl, toastCtrl, alertCtrl, loadingCtrl, formBuilder, auth, events, storage) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.auth = auth;
        this.events = events;
        this.storage = storage;
    }
    LoginPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    LoginPage.prototype.ngOnInit = function () {
        this.onLoginForm = this.formBuilder.group({
            'email': [null, Validators.compose([
                    Validators.required
                ])],
            'password': [null, Validators.compose([
                    Validators.required
                ])]
        });
    };
    LoginPage.prototype.forgotPass = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Forgot Password?',
                            message: 'Enter you email address to send a reset link password.',
                            inputs: [
                                {
                                    name: 'email',
                                    type: 'email',
                                    placeholder: 'Email'
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        console.log('Confirm Cancel');
                                    }
                                }, {
                                    text: 'Confirm',
                                    handler: function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                        var loader;
                                        var _this = this;
                                        return tslib_1.__generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0: return [4 /*yield*/, this.loadingCtrl.create({
                                                        duration: 2000
                                                    })];
                                                case 1:
                                                    loader = _a.sent();
                                                    loader.present();
                                                    loader.onWillDismiss().then(function (l) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                                        var toast;
                                                        return tslib_1.__generator(this, function (_a) {
                                                            switch (_a.label) {
                                                                case 0: return [4 /*yield*/, this.toastCtrl.create({
                                                                        showCloseButton: true,
                                                                        message: 'Email was sended successfully.',
                                                                        duration: 3000,
                                                                        position: 'bottom'
                                                                    })];
                                                                case 1:
                                                                    toast = _a.sent();
                                                                    toast.present();
                                                                    return [2 /*return*/];
                                                            }
                                                        });
                                                    }); });
                                                    return [2 /*return*/];
                                            }
                                        });
                                    }); }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.goToRegister = function () {
        this.navCtrl.navigateRoot('/register');
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        if (!this.username || !this.password) {
            this.showError("Please enter username and password.");
            return;
        }
        this.auth.login(this.username, this.password).subscribe(function (res) {
            if (res.IsAuthenticated) {
                Globals.Session.TokenId = res.TokenId;
                Globals.Session.IsAuthenticated = res.IsAuthenticated;
                //User Details
                Globals.Session.User.UserId = res.User.UserId;
                Globals.Session.User.Username = res.User.Username;
                Globals.Session.User.DisplayName = res.User.DisplayName;
                Globals.Session.User.Email = res.User.Email;
                Globals.Session.User.Source = res.User.Source;
                Globals.Session.User.RoleCode = res.User.RoleCode;
                Globals.Session.User.RoleName = res.User.RoleName;
                Globals.Session.User.CompanyId = res.User.CompanyId;
                Globals.Session.User.CompanyCode = res.User.CompanyCode;
                Globals.Session.User.CompanyName = res.User.CompanyName;
                Globals.Session.User.BranchId = res.User.BranchId;
                Globals.Session.User.BranchCode = res.User.BranchCode;
                Globals.Session.User.BranchName = res.User.BranchName;
                _this.storage.set(Globals.QueueSession, {
                    User: res.User,
                    TokenId: res.TokenId,
                    IsAuthenticated: res.IsAuthenticated
                });
                _this.events.publish("user:login");
                switch (res.User.RoleCode) {
                    case "FDO": {
                        _this.navCtrl.navigateRoot(['/queue-list']);
                        break;
                    }
                    case "CSA": {
                        _this.navCtrl.navigateRoot(['/queue-call']);
                        break;
                    }
                    default: {
                        _this.navCtrl.navigateRoot(['/edit-profile']);
                        break;
                    }
                }
            }
            else {
                _this.showError("Unable to gain access for user" + _this.username + ".");
            }
        }, function (err) {
            _this.showError(err);
        });
    };
    LoginPage.prototype.showError = function (err) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var errorText, toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Globals.isObject(err)) {
                            if (Globals.isObject(err.error)) {
                                errorText = "Unable to connect to server.";
                            }
                            else {
                                errorText = err.error;
                            }
                        }
                        else {
                            errorText = err;
                        }
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: errorText,
                                duration: 2000,
                                color: "danger"
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = tslib_1.__decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            ToastController,
            AlertController,
            LoadingController,
            FormBuilder,
            AuthService,
            Events,
            Storage])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.page.js.map
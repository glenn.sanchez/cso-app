import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { QueueEditPage } from './queue-edit.page';
var routes = [
    {
        path: '',
        component: QueueEditPage
    }
];
var QueueEditPageModule = /** @class */ (function () {
    function QueueEditPageModule() {
    }
    QueueEditPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [QueueEditPage]
        })
    ], QueueEditPageModule);
    return QueueEditPageModule;
}());
export { QueueEditPageModule };
//# sourceMappingURL=queue-edit.module.js.map
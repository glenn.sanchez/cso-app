import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { QueueService } from 'src/app/services/queue.service';
import { Globals } from 'src/app/services/globals.service';
import { ToastController, MenuController, NavController, LoadingController } from '@ionic/angular';
var SumItem = /** @class */ (function () {
    function SumItem() {
    }
    return SumItem;
}());
var QueueCallPage = /** @class */ (function () {
    function QueueCallPage(navCtrl, router, qservice, toastCtrl, menuCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.router = router;
        this.qservice = qservice;
        this.toastCtrl = toastCtrl;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.srv = new SumItem();
        this.mec = new SumItem();
        this.bnp = new SumItem();
        this.col = new SumItem();
        this.hld = new SumItem();
        this.tow = new SumItem();
        this.cmp = new SumItem();
    }
    QueueCallPage.prototype.ngOnInit = function () {
    };
    QueueCallPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
        this.loadData();
    };
    QueueCallPage.prototype.loadData = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loader;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: "Loading..."
                        })];
                    case 1:
                        loader = _a.sent();
                        return [4 /*yield*/, loader.present()];
                    case 2:
                        _a.sent();
                        this.qservice.getQueueSummary().subscribe(function (items) {
                            loader.dismiss();
                            _this.mec = items.find(function (x) { return x.GroupCode == 'MEC'; });
                            _this.bnp = items.find(function (x) { return x.GroupCode == 'BNP'; });
                            _this.col = items.find(function (x) { return x.GroupCode == 'COL'; });
                            _this.hld = items.find(function (x) { return x.GroupCode == 'HLD'; });
                            _this.srv = items.find(function (x) { return x.GroupCode == 'SRV'; });
                            _this.tow = items.find(function (x) { return x.GroupCode == 'TOW'; });
                            _this.cmp = items.find(function (x) { return x.GroupCode == 'CMP'; });
                        }, function (err) {
                            loader.dismiss();
                            _this.showError(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    QueueCallPage.prototype.nextQueue = function (type) {
        this.router.navigate(['/queue-call-edit'], {
            queryParams: { type: type }
        });
    };
    QueueCallPage.prototype.showError = function (err) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var errorText, toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Globals.isObject(err.error)) {
                            errorText = "Unable to connect to server.";
                        }
                        else {
                            errorText = err.error;
                        }
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: errorText,
                                duration: 2000,
                                color: "danger"
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    QueueCallPage.prototype.viewSumItems = function (groupcd, statuscd) {
        this.router.navigate(['/queue-list'], {
            queryParams: {
                groupcd: groupcd,
                statuscd: statuscd,
                rootcaller: '/queue-call'
            }
        });
    };
    QueueCallPage.prototype.goToProfile = function () {
        this.navCtrl.navigateForward('edit-profile');
    };
    QueueCallPage = tslib_1.__decorate([
        Component({
            selector: 'queue-call',
            templateUrl: './queue-call.page.html',
            styleUrls: ['./queue-call.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            Router,
            QueueService,
            ToastController,
            MenuController,
            LoadingController])
    ], QueueCallPage);
    return QueueCallPage;
}());
export { QueueCallPage };
//# sourceMappingURL=queue-call.page.js.map
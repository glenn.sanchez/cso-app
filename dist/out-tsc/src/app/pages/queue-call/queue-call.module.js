import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { QueueCallPage } from './queue-call.page';
var routes = [
    {
        path: '',
        component: QueueCallPage
    }
];
var QueueCallPageModule = /** @class */ (function () {
    function QueueCallPageModule() {
    }
    QueueCallPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [QueueCallPage]
        })
    ], QueueCallPageModule);
    return QueueCallPageModule;
}());
export { QueueCallPageModule };
//# sourceMappingURL=queue-call.module.js.map
import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { QueueCallEditPage } from './queue-call-edit.page';
var routes = [
    {
        path: '',
        component: QueueCallEditPage
    }
];
var QueueCallEditPageModule = /** @class */ (function () {
    function QueueCallEditPageModule() {
    }
    QueueCallEditPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [QueueCallEditPage]
        })
    ], QueueCallEditPageModule);
    return QueueCallEditPageModule;
}());
export { QueueCallEditPageModule };
//# sourceMappingURL=queue-call-edit.module.js.map
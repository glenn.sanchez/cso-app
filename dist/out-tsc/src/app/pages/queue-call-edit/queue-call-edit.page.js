import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController, ModalController, AlertController } from '@ionic/angular';
import { QueueService } from '../../services/queue.service';
import { ImagePage } from '../modal/image/image.page';
import { Globals } from 'src/app/services/globals.service';
import { VehInfo } from 'src/app/interfaces/queue';
var QueueCallEditPage = /** @class */ (function () {
    function QueueCallEditPage(navCtrl, loadingCtrl, toastCtrl, modalCtrl, qservice, route, router, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.qservice = qservice;
        this.route = route;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.blockQueue = true;
        this.customPopoverOptions = {
            header: 'Service Type',
            //subHeader: 'Select Appointment Type',
            message: 'State the purpose of your visit'
        };
        this.imgpath = Globals.EndPoint;
        this.item = this.qservice.getQueueTemplate();
        this.vehinfo = new VehInfo();
        this.services = Globals.ServiceTypes;
        this.route.queryParams.subscribe(function (res) {
            //this.callerroute = res.CallerRoute;
            _this.grouptype = res.type;
            if (Globals.current_queue != null) {
                _this.item = Globals.current_queue;
            }
            else {
                _this.qservice.getNextQueueItem(_this.grouptype).subscribe(function (item) {
                    _this.item = item[0];
                    console.log(_this.item);
                }, function (err) {
                    console.log(err);
                    _this.showError(err);
                });
            }
            _this.setServiceTypeName();
        });
        //this.itemId = Number(this.route.snapshot.paramMap.get('id'));
        //console.log(this.services);
    }
    QueueCallEditPage.prototype.selectService = function ($event) {
        var _this = this;
        console.log($event.detail.value, this.item.ServiceTypeId);
        var srv = this.services.find(function (x) { return x.Id == _this.item.ServiceTypeId; });
        if (srv != null) {
            this.srvtypename = srv.TypeName;
        }
        // console.log("this.srvtypename",this.srvtypename);
        //this.item.ServiceTypeId = $event.detail.value;
        //this.setServiceTypeName();
        //console.log(this.sel_service);
        /*
        if (this.sel_service){
          var srv = this.services.find(x => x.Id == this.sel_service);
          this.sel_service = srv?srv.TypeName:null;
        }
        */
    };
    QueueCallEditPage.prototype.setServiceTypeName = function () {
        var _this = this;
        if (this.item.ServiceTypeId == null) {
            this.item.ServiceTypeId = this.services[0].Id;
        }
        var srv = this.services.find(function (x) { return x.Id == _this.item.ServiceTypeId; });
        this.srvtypename = srv.TypeName;
    };
    QueueCallEditPage.prototype.ngOnInit = function () {
    };
    QueueCallEditPage.prototype.ionViewWillEnter = function () {
    };
    QueueCallEditPage.prototype.validateEntries = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var ok, toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ok = false;
                        //console.log(this.item.regno);
                        if (this.item.RegNo) {
                            ok = false;
                        }
                        if (!!ok) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.toastCtrl.create({
                                showCloseButton: true,
                                //cssClass: 'bg-profile',
                                message: 'Some of required fields are missing.',
                                duration: 3000,
                                position: 'bottom'
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        _a.label = 2;
                    case 2: return [2 /*return*/, ok];
                }
            });
        });
    };
    QueueCallEditPage.prototype.sendData = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var ok, loader;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ok = this.validateEntries();
                        if (!ok)
                            return [2 /*return*/];
                        return [4 /*yield*/, this.loadingCtrl.create({
                                duration: 2000
                            })];
                    case 1:
                        loader = _a.sent();
                        return [4 /*yield*/, loader.present()];
                    case 2:
                        _a.sent();
                        loader.onWillDismiss().then(function (l) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var toast;
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                                            showCloseButton: true,
                                            cssClass: 'bg-profile',
                                            message: 'Your profile was was edited!',
                                            duration: 3000,
                                            position: 'bottom'
                                        })];
                                    case 1:
                                        toast = _a.sent();
                                        toast.present();
                                        this.navCtrl.navigateForward('/queue-list');
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    QueueCallEditPage.prototype.presentImage = function (image) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: ImagePage,
                            componentProps: { value: image }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    QueueCallEditPage.prototype.editCSA = function () {
        Globals.current_queue = this.item;
        //this.navCtrl.navigateForward('/csa-list');
        //this.router.navigate(['/csa-list'], {
        //queryParams: {CallerRoute:this.callerroute ,Id:this.itemId}
        //});
    };
    QueueCallEditPage.prototype.closeModal = function () {
        Globals.current_queue = null;
        this.navCtrl.navigateBack("/queue-call");
    };
    QueueCallEditPage.prototype.confirm = function (type) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (type) {
                    case "Keydrop": {
                        //this.item.IsKeyDrop=true;
                        this.confirmAction("Keydrop", "Receive car key?", { type: type });
                        break;
                    }
                    case "TestDrive": {
                        //this.item.IsTestDrive=true;
                        this.confirmAction("Test Drive", "Mark this queue as test drive?", { type: type });
                        break;
                    }
                    case "Delete": {
                        //this.item.IsDeleted=true;
                        this.confirmAction("Delete Queue", "Do you really want to delete this queue?", { type: type });
                        break;
                    }
                    case "Complete": {
                        //this.item.Status="Completed";
                        this.confirmAction("Confirm Complete", "Finish serving customer?", { type: type });
                        break;
                    }
                    case "On-Hold": {
                        //this.item.Status="On-Hold";
                        this.confirmAction("Confirm to Put On-Hold", "Put this queue on-hold?", { type: type });
                        break;
                    }
                    default: {
                        this.confirmAction("Confirm Save", "Save/Update queue details?", { type: type });
                        break;
                    }
                }
                return [2 /*return*/];
            });
        });
    };
    QueueCallEditPage.prototype.showError = function (err) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var errorText, toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Globals.isObject(err.error)) {
                            errorText = "Unable to connect to server.";
                        }
                        else {
                            errorText = err.error;
                        }
                        console.log(errorText);
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: errorText,
                                duration: 2000,
                                color: "danger"
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    QueueCallEditPage.prototype.compareWithFn = function (o1, o2) {
        console.log(o1, o2);
        return o1 && o2 ? o1.id === o2.id : o1 === o2;
    };
    ;
    QueueCallEditPage.prototype.checkValue = function (event) {
        //console.log(event.detail.value,this.item.ServiceTypeId);
        this.item.ServiceTypeId = event.detail.value;
    };
    QueueCallEditPage.prototype.getVehicleInfo = function () {
        var _this = this;
        this.qservice.getVehicleInfo(this.item.RegNo, this.item.MobileNo).subscribe(function (item) {
            //console.log(item.length);      
            if (item.length > 0) {
                _this.vehinfo = item[0];
            }
            else {
                _this.vehinfo = new VehInfo();
            }
            _this.item.CustomerName = _this.vehinfo.CustomerName;
            _this.item.ServiceDate = _this.vehinfo.ApptDate;
            _this.item.CSAUserId = _this.vehinfo.CSAUserId;
            _this.item.CSAName = _this.vehinfo.CSAName;
        }, function (err) {
            _this.showError(err);
        });
    };
    QueueCallEditPage.prototype.confirmAction = function (title, message, option) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: title,
                            message: message,
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                }, {
                                    text: 'Ok',
                                    handler: function () {
                                        _this.saveData(option);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    QueueCallEditPage.prototype.saveData = function (option) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loader;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        switch (option.type) {
                            case "Keydrop": {
                                this.item.IsKeyDrop = true;
                                break;
                            }
                            case "Testdrive": {
                                this.item.IsTestDrive = true;
                                break;
                            }
                            case "Delete": {
                                this.item.IsDeleted = true;
                                break;
                            }
                            case "Complete": {
                                this.item.Status = "Completed";
                                break;
                            }
                            case "On-Hold": {
                                this.item.Status = "On-Hold";
                                break;
                            }
                        }
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: "Saving Changes..."
                            })];
                    case 1:
                        loader = _a.sent();
                        return [4 /*yield*/, loader.present()];
                    case 2:
                        _a.sent();
                        this.qservice.setQueueItem(this.item).subscribe(function (res) {
                            loader.dismiss();
                            Globals.current_queue = null;
                            _this.navCtrl.navigateBack("/queue-call");
                        }, function (err) {
                            loader.dismiss();
                            _this.showError(err.error);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    QueueCallEditPage = tslib_1.__decorate([
        Component({
            selector: 'queue-new',
            templateUrl: './queue-call-edit.page.html',
            styleUrls: ['./queue-call-edit.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            LoadingController,
            ToastController,
            ModalController,
            QueueService,
            ActivatedRoute,
            Router,
            AlertController])
    ], QueueCallEditPage);
    return QueueCallEditPage;
}());
export { QueueCallEditPage };
//# sourceMappingURL=queue-call-edit.page.js.map
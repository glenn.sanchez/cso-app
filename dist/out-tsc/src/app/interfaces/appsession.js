var AppSession = /** @class */ (function () {
    function AppSession() {
        this.IsAuthenticated = false;
        this.TokenId = null;
        this.User = new User();
    }
    return AppSession;
}());
export { AppSession };
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());
//# sourceMappingURL=appsession.js.map
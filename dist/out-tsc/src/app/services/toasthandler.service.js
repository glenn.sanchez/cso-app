import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Globals } from './globals.service';
var ToasthandlerService = /** @class */ (function () {
    function ToasthandlerService(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ToasthandlerService.prototype.showError = function (error) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var errorText, toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Globals.isObject(error)) {
                            errorText = "Unable to connect to server.";
                        }
                        else {
                            errorText = error;
                        }
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: errorText,
                                duration: 2000,
                                color: "danger"
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ToasthandlerService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [ToastController])
    ], ToasthandlerService);
    return ToasthandlerService;
}());
export { ToasthandlerService };
//# sourceMappingURL=toasthandler.service.js.map
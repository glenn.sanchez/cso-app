import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
//import { HttpClient,Request} from 'selenium-webdriver/http';
import { HttpClient } from '@angular/common/http';
import { Globals } from './globals.service';
var QueueService = /** @class */ (function () {
    function QueueService(http) {
        this.http = http;
        this.endpoint = Globals.EndPoint;
    }
    QueueService.prototype.getQueueItems = function (IsTestDrive, IsValet, groupCode, statusCode) {
        var param = {
            TokenId: Globals.Session.TokenId,
            IsTestDrive: IsTestDrive,
            IsValet: IsValet,
            GroupCode: groupCode,
            StatusCode: statusCode
        };
        return this.http.post(this.endpoint + "/api/queue/get", param);
    };
    QueueService.prototype.getQueueItemDetail = function (itemId) {
        var param = {
            TokenId: Globals.Session.TokenId,
            Id: itemId
        };
        return this.http.post(this.endpoint + "/api/queue/get", param);
    };
    QueueService.prototype.getNextQueueItem = function (GroupCode) {
        var param = {
            TokenId: Globals.Session.TokenId,
            CSAUserId: Globals.Session.User.UserId,
            ServiceGroupCode: GroupCode
        };
        return this.http.post(this.endpoint + "/api/queue/next", param);
    };
    QueueService.prototype.getQueueSummary = function () {
        var param = {
            TokenId: Globals.Session.TokenId,
            CSAUserId: Globals.Session.User.UserId,
        };
        return this.http.post(this.endpoint + "/api/queue/summary", param);
    };
    QueueService.prototype.getQueueTemplate = function () {
        var item = {
            Id: null,
            QueueDate: null,
            CompanyId: null,
            BranchId: null,
            AppointmentId: null,
            QueueNo: null,
            ServiceGroupId: null,
            GroupCode: null,
            GroupName: null,
            ServiceTypeId: null,
            TypeCode: null,
            TypeName: null,
            ServiceDate: null,
            RegNo: null,
            ChassisNo: null,
            CustomerName: null,
            MobileNo: null,
            CSAUserId: null,
            CSAName: "Next Available CSA",
            ArrivedOn: null,
            RONo: null,
            RODate: null,
            PictureUrl: null,
            IsVIP: null,
            IsPriorityPlus: null,
            IsKeyDrop: null,
            IsTestDrive: null,
            IsDeleted: null,
            RefQueueId: null,
            InsertDate: null,
            InsertUserId: null,
            InsertUserName: null,
            UpdateDate: null,
            ServeStartOn: null,
            ServeEndOn: null,
            UpdateUserId: null,
            UpdateUserName: null,
            Status: null,
            ApptId: null,
            ApptTime: null,
            QueuedOn: null,
            ServeTime: null,
            TimeWaited: null,
            Remarks: null
        };
        return item;
    };
    QueueService.prototype.setQueueItem = function (item) {
        var param = {
            "TokenId": Globals.Session.TokenId,
            "Id": item.Id,
            "ServiceTypeId": item.ServiceTypeId,
            "CSAUserId": item.CSAUserId,
            "CustomerName": item.CustomerName,
            "MobileNo": item.MobileNo,
            "RegNo": item.RegNo,
            "ChassisNo": item.ChassisNo,
            "ServiceDate": item.ServiceDate,
            "RONo": item.RONo,
            "RODate": item.RODate,
            "AppointmentId": item.AppointmentId ? item.AppointmentId : null,
            "IsVIP": item.IsVIP,
            "IsKeydrop": item.IsKeyDrop,
            "IsTestdrive": item.IsTestDrive,
            "IsDeleted": item.IsDeleted,
            "Remarks": item.Remarks,
            "RefQueueId": item.RefQueueId ? item.RefQueueId : null,
            "Status": item.Status
        };
        return this.http.post(this.endpoint + "/api/queue/set", param);
    };
    QueueService.prototype.getCSAItems = function () {
        var param = {
            TokenId: Globals.Session.TokenId
        };
        return this.http.post(this.endpoint + "/api/onlinecsa/get", param);
    };
    QueueService.prototype.getVehicleInfo = function (regNo, mobileNo) {
        var param = {
            TokenId: Globals.Session.TokenId,
            RegNo: regNo,
            MobileNo: mobileNo
        };
        return this.http.post(this.endpoint + "/api/vehicle/info/get", param);
    };
    QueueService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], QueueService);
    return QueueService;
}());
export { QueueService };
//# sourceMappingURL=queue.service.js.map
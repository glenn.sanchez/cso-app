import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from './globals.service';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';
import { Device } from '@ionic-native/device/ngx';
import { Platform } from '@ionic/angular';
var AuthService = /** @class */ (function () {
    function AuthService(http, platform, device, networkInterface) {
        var _this = this;
        this.http = http;
        this.platform = platform;
        this.device = device;
        this.networkInterface = networkInterface;
        this.endpoint = Globals.EndPoint;
        if (this.platform.is("cordova")) {
            this._manufacturer = this.device.manufacturer;
            this._platform = this.device.platform;
            this._uuid = this.device.uuid;
            this._model = this.device.model;
            this._serial = this.device.serial;
            this.networkInterface.getWiFiIPAddress().then(function (ip) {
                _this._ipaddress = ip;
            });
        }
    }
    AuthService.prototype.login = function (username, password) {
        var param = {
            UserName: username,
            Password: password,
            Manufacturer: this._manufacturer,
            Platform: this._platform,
            DeviceId: this._uuid,
            Model: this._model,
            SerialNo: this._serial,
            IPAddress: this._ipaddress
        };
        return this.http.post(this.endpoint + "/api/security/login", param);
    };
    AuthService.prototype.setPassword = function (oldpassword, newpassword) {
        var param = {
            "TokenId": Globals.Session.TokenId,
            "OldPassword": oldpassword,
            "NewPassword": newpassword
        };
        return this.http.post(this.endpoint + "/api/security/changepassword", param);
    };
    AuthService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient,
            Platform,
            Device,
            NetworkInterface])
    ], AuthService);
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.service.js.map
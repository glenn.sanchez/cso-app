import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from './globals.service';
var ConfigService = /** @class */ (function () {
    function ConfigService(http) {
        this.http = http;
        this.endpoint = Globals.EndPoint;
    }
    ConfigService.prototype.initSetup = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                this.getUserServices().subscribe(function (items) {
                    Globals.ServiceTypes = items;
                });
                return [2 /*return*/];
            });
        });
    };
    ConfigService.prototype.getUserServices = function () {
        var param = {
            TokenId: Globals.Session.TokenId
        };
        return this.http.post(this.endpoint + "/api/userservices/get", param);
    };
    ConfigService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], ConfigService);
    return ConfigService;
}());
export { ConfigService };
//# sourceMappingURL=config.service.js.map
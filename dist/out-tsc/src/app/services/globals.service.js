import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var Globals = /** @class */ (function () {
    function Globals() {
    }
    Globals.isObject = function (val) {
        if (val === null) {
            return false;
        }
        return ((typeof val === 'function') || (typeof val === 'object'));
    };
    //public static EndPoint      :string = "http://localhost:53892";    
    Globals.EndPoint = "https://dev.simedarbymotors.com/care";
    Globals.ServiceTypes = [];
    Globals.QueueSession = "QueueSession";
    Globals = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], Globals);
    return Globals;
}());
export { Globals };
//# sourceMappingURL=globals.service.js.map